extends Spatial

onready var main_node = get_tree().get_root().get_node("Main")
var shroom01 = preload("res://objects/shrooms01.tscn")
var shroom02 = preload("res://objects/shrooms02.tscn")

var max_items = 18
var x_min_max = Vector2(-13, 13)
var z_min_max = Vector2(-9, 40)


func spawn_rand_itam():
	var item_list = [
		shroom01,
		shroom02,
		]
	var n = 100
	var place_found = false
	while !place_found and n > 0:
		var x_pos = randf() * (x_min_max.y - x_min_max.x) + x_min_max.x
		var z_pos = randf() * (z_min_max.y - z_min_max.x) + z_min_max.x
		var y_pos = $RayCast.global_transform.origin.y
		$RayCast.global_transform.origin = Vector3(x_pos, y_pos, z_pos)
		$RayCast.force_raycast_update()
		if $RayCast.is_colliding() and $RayCast.get_collider().is_in_group("floor"):
			place_found = true
			var item_object = item_list[randi() % item_list.size()].instance()
			$Items.add_child(item_object)
			item_object.global_transform.origin = $RayCast.get_collision_point()
			main_node.spawn_effect("pop", item_object.global_transform.origin)
		n -= 1

func _ready():
	randomize()
	for i in range(max_items / 2):
		spawn_rand_itam()
