extends Spatial

onready var main_node = get_tree().get_root().get_node("Main")
var items = {}
var money = 0

func update_item_view():
	main_node.update_item_view(money, items)
	
	
func item_purchased(item_name, price, amount):
	var result = false
	if money - price * amount >= 0:
		money -= price	* amount
		if items.has(item_name):
			items[item_name] += amount
		else:
			items[item_name] = amount
		result = true
	else:
		result = false
	update_item_view()
	return result


func item_sell(item_name, price, amount):
	var result = false
	if items.has(item_name) and items[item_name] >= amount:
		items[item_name] -= amount
		money += price	* amount
		result = true
	else:
		result = false
	update_item_view()
	return result


func item_pickup(item_name, amount):
	var result = item_purchased(item_name, 0, amount)
	update_item_view()
	return result


func item_drop(item_name, amount):
	var result = item_sell(item_name, 0, amount)
	update_item_view()
	return result
