extends Camera

onready var main_node = get_tree().get_root().get_node("Main")
const MIN_Y_POS = -13.0
const MAX_Y_POS = 0.0
var y_step = 1.0

func move_camera(value):
	transform.origin.y = clamp(transform.origin.y + value, MIN_Y_POS, MAX_Y_POS)
	look_at(main_node.player.global_transform.origin, Vector3.UP)


func _input(event):
	if event.is_action_pressed("camera_up"):
		move_camera(-y_step)
	elif event.is_action_pressed("camera_down"):
		move_camera(y_step)
