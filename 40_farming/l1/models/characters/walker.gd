extends Spatial


func _ready():
	$AnimationPlayer.get_animation("idle").loop = true
	$AnimationPlayer.get_animation("walk").loop = true
	$AnimationPlayer.play("idle")


func play_animation(animation_name):
	$AnimationPlayer.play(animation_name)
