extends Spatial

func spawn_effect(effect_name, pos):
	var effect_file_path = "res://effects/" + effect_name + ".tscn"
	var effect_file = load(effect_file_path)
	var effect = effect_file.instance()
	add_child(effect)
	effect.global_transform.origin = pos
