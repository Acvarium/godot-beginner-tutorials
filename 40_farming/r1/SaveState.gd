extends Node

var save_filename = "user://save_game.save"

func _ready():
	pass
	load_game()

func save_game():
	var save_file = File.new()
	save_file.open(save_filename, File.WRITE)
	var saved_nodes = get_tree().get_nodes_in_group("saved")
	for node in saved_nodes:
		var node_details = node.get_save_stats()
		save_file.store_line(to_json(node_details))
	save_file.close()


func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		save_game()


func load_game():
	var save_file = File.new()
	if not save_file.file_exists(save_filename):
		return
		
	var saved_nodes = get_tree().get_nodes_in_group("spawned")
	for node in saved_nodes:
		node.queue_free()
	
	save_file.open(save_filename, File.READ)
	while save_file.get_position() < save_file.get_len():
		var node_data = parse_json(save_file.get_line())
		if "scenepath" in node_data.keys():
			get_node(node_data.scenepath).call_deferred('load_save_stats', node_data) 
