extends Spatial
onready var main_node = get_tree().get_root().get_node("Main")
var items = {}
var money = 0

func _ready():
	update_item_view()

func item_purchased(item_name, price, amount):
	var result = false
	if money - price * amount >= 0:
		money -= price	* amount
		if items.has(item_name):
			items[item_name] += amount
		else:
			items[item_name] = amount
		result = true
	else:
		result = false
	update_item_view()
	return result


func item_sell(item_name, price, amount):
	var result = false
	if items.has(item_name) and items[item_name] >= amount:
		items[item_name] -= amount
		money += price	* amount
		result = true
	else:
		result = false
	update_item_view()
	return result


func item_pickup(item_name, amount):
	var result = item_purchased(item_name, 0, amount)
	update_item_view()
	return result
	

func item_drop(item_name, amount):
	var result = item_sell(item_name, 0, amount)
	update_item_view()
	return result
	

func update_item_view():
	main_node.update_item_view(money, items)


func get_save_stats():
	var save_stats = {}
	save_stats.scenepath = get_path()
	save_stats.money = money
	
	for k in items.keys():
		save_stats["item." + k] = items[k]
	return save_stats

func load_save_stats(stats):
	money = stats.money
	for k in stats.keys():
		if "item." in k:
			items[k.split('.')[1]] = stats[k]
	update_item_view()
