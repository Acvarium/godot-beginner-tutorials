extends Spatial
export var offset_time_max = 0.5
export var pitch_offset = 0.5


func _ready():
	if has_node("Offset"):
		$Offset.wait_time = randf() * offset_time_max
		$Offset.start()
	elif has_node("AudioStreamPlayer3D"):
		$AudioStreamPlayer3D.play()
	if has_node("AudioStreamPlayer3D"):
		$AudioStreamPlayer3D.pitch_scale = 1.0 + randf() * pitch_offset - (pitch_offset / 2)


func _on_Timer_timeout():
	queue_free()


func _on_Offset_timeout():
	if has_node("AudioStreamPlayer3D"):
		$AudioStreamPlayer3D.play()
