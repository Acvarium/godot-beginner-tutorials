extends KinematicBody

onready var main_node = get_tree().get_root().get_node("Main")
export (NodePath) var camera_holder_path
export (NodePath) var item_holder_path
var item_holder = null

# Перечислювач, що визначає стан персонажу
enum Actions {IDLE, WALK, PICKUP, WATER}
var action = Actions.IDLE

# Позиція, в яку треба переміститись
var next_point = Vector3()
# Вектор швидкості
var velocity = Vector3()
# Прискорення вільного падіння
var gravity = -9.8 * 3
# Максимальний кут сковзання по поверхні
const MAX_SLOPE_ANGLE = 35.0
# Максимальна швидкість бігу
const MAX_RUN_SPEED = 10.0
# Максимальна швидкість ходьби
var MAX_SPEED = 3.0
# Прискорення
const ACCEL = 2.0
# Сповільнення
const DEACCEL = 15.0
# Швидкість обертання
var turn_speed = 6.0

var object_to_pickup = null

func _ready():
	if has_node(item_holder_path):
		item_holder = get_node(item_holder_path)
	$CameraMover.remote_path = $CameraMover.get_path_to(get_node(camera_holder_path))

# Функція, за допомогою якої визначається позиція, до якої треба рухатись персонажу
func goto_point(point):
	next_point = point
	if has_node("walker"):
		$walker.play_animation("walk")
	if action == Actions.IDLE:
		action = Actions.WALK


func walk_and_pickup_object(obj):
	if is_instance_valid(obj):
		action = Actions.PICKUP
		object_to_pickup = obj
		goto_point(obj.global_transform.origin)

func walk_to_point(point):
	object_to_pickup = null
	action = Actions.WALK
	goto_point(point)
	

func object_pickup(obj):
	if !is_instance_valid(item_holder):
		return
	if is_instance_valid(obj):
		var amount = 1
		if obj.has_method("get_amount"):
			amount = obj.get_amount()
		item_holder.item_pickup(obj.get_item_name(), amount)
		main_node.spawn_effect("pop2", obj.global_transform.origin)
		obj.queue_free()


func _physics_process(delta):
#	Якщо обрана якась дія, тоді виконати переміщення до позиції її виконання
	var to_walk = action != Actions.IDLE
	walk(delta, to_walk, next_point)


# Функція переміщення до потрібної точки в світі
func walk(delta, to_walk, target_pos):
#	Вектор, в який беде зберігатись напрямок руху
	var direction = Vector3()
#	Розрахунок дистанції до цілі
	var target_dist = global_transform.origin.distance_to(target_pos)
#	Якщо ця дистанція менша за визначену величину, не переміщатись
	if target_dist < 0.4:
		to_walk = false
		if action == Actions.PICKUP and is_instance_valid(object_to_pickup):
			object_pickup(object_to_pickup)
		if has_node("walker"):
			$walker.play_animation("idle")
	if to_walk:
#		Розрахунок очікуваного нормалізованого напрямку руху по горизонталі
		direction = (global_transform.origin.direction_to(target_pos) * Vector3(1,0,1)).normalized()
#		Зберігаємо базис персонажа, що дозволить обрати напрямок відносно нього
		var aim = global_transform.basis
#		Якщо ціль руху десь перед персонажем, тоді новий напрям руху, це туди, куди персонаж дивиться
#		якщо ж ціль позаду, тоді персонаж не повинен переміщатись
		direction = -aim.z if (direction.dot(-aim.z) > 0) else Vector3()
		
#		Дістаємо з базису напрямок праворуч відносно персонажу, та розраховуємо скалярний добуток до напрямку на ціль
		var aim_right_dot = aim.x.dot(global_transform.origin.direction_to(target_pos).normalized())
#		Визначаємо скалярний добуток відносно погляду персонажу вперед, та напрямку на ціль
		var aim_dot = (-aim.z).dot(global_transform.origin.direction_to(
			target_pos * Vector3(1,global_transform.origin.y,1)).normalized())
		
#		Якщо величина скалярного добутку напрямку погляду до напрямку на ціль менша за 0.99
		if (aim_dot < 0.99):
#			тоді визначаємо куди потрібно повернути, ліворуч чи праворуч
			if aim_right_dot < 0:
#				повертаємо персонажа ліворуч, на величину швидкості повороту
				transform.basis = transform.basis.rotated(Vector3(0.0, 1.0, 0.0), turn_speed * delta)
			else:
#				повертаємо персонажа праворуч, на величину швидкості повороту
				transform.basis = transform.basis.rotated(Vector3(0.0, 1.0, 0.0), -turn_speed * delta)
		else:
#			якщо ж (aim_dot >= 0.99), тоді гравець уже майже дивиться в напрямку цілі
#			і в цьому випадку ми його довертаємо так, щоб він дивився прямо на ціль
			look_at(Vector3(target_pos.x, global_transform.origin.y, target_pos.z), Vector3.UP)

#	Якщо на поверхні землі
	if is_on_floor():
#		Отримуємо вектор нормалі до поверхні, на якій стоїмо
		var n = $tail.get_collision_normal()
#		З вектора нормалі визначаємо кур поверхні
		var floor_angle = rad2deg(acos(n.dot(Vector3(0,1,0))))
#		А тоді, якщо кут перевищує той, який ми визначили
		if floor_angle > MAX_SLOPE_ANGLE:
#			додаємо прискорення вільного падіння, що приведе до сковзання з поверхні
			velocity.y += gravity * delta
	else:
#		Якщо ж персонаж не на поверхні землі, тоді додаємо прискорення вільного падіння 
#		до вертикальної складової вектора швидкості
		velocity.y += gravity * delta
		
#	Створюємо тимчасовий вектор швидкості руху, в який записуємо лише горизонтальні складові 
#	з збереженого вектора швидкості руху
	var temp_velocity = velocity * Vector3(1,0,1)
#	визначаємо швидкість руху, у відповідності до дистанції до цілі
	var speed = MAX_SPEED if (target_dist < 6) else MAX_RUN_SPEED
#	розраховуємо цільовий вектор, як добуток визначеного напрямку на швидкість
	var target = direction * speed
#	визначаємо значення прискорення як величину константи прискорення, якщо рухаємось вперед, 
#	або величину константи сповільнення, якщо рухаємось назад
	var acceleration = ACCEL if (direction.dot(temp_velocity) > 0) else DEACCEL
#	в тимчасовий вектор швидкості руху розріховуємо інтеопольовану величину
#	що приведе до плавного набирання швидкості
	temp_velocity = temp_velocity.linear_interpolate(target, acceleration * delta)
#	записуємо розраховані значення по горизонталі, з тимчасового вектора швидкості руху, в постійний
	velocity.x = temp_velocity.x
	velocity.z = temp_velocity.z
#	викликаємо функцію переміщення з ковзанням, якій передаємо вектор швидкості руху, на обраний нами напрямок вгору
#	ця функція повертає залишок від вектора швидкості, після здійсьненого руху. 
#	Тобто, якщо в результаті руху відбулось зіткнення з поверхнею, вектор буде змінено
#	його і буде повернено та збережено для використання в наступних циклах
	velocity = move_and_slide(velocity, Vector3(0,1,0))


func get_save_stats():
	var save_stats = {}
	save_stats.scenepath = get_path()
	save_stats.x_pos = global_transform.origin.x
	save_stats.y_pos = global_transform.origin.y
	save_stats.z_pos = global_transform.origin.z
	return save_stats

func load_save_stats(stats):
	global_transform.origin = Vector3(stats.x_pos, stats.y_pos, stats.z_pos)
