extends "res://objects/Pickable.gd"

func _ready():
	randomize()
	random_shroom()
	
	
func random_shroom():
	var selected_shroom = randi() % $shrooms.get_child_count()
	for i in range($shrooms.get_child_count()):
		$shrooms.get_child(i).visible = i == selected_shroom
	amount = 1
	if selected_shroom > 8:
		amount = 3
