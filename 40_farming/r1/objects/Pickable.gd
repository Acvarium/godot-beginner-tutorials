extends KinematicBody

export var item_name = ""
export var amount = 1

func get_item_name():
	return item_name
	
func get_amount():
	return amount


func get_stats():
	return {"item_name": item_name, "amount": amount}
