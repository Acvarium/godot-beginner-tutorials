extends KinematicBody

func get_target_position():
	if has_node("TargetPosition"):
		return $TargetPosition.global_transform.origin
	return global_transform.origin


func get_stats():
	return	{'name': "interactable", 'level': 1.5}
