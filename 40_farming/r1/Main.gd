extends Spatial
onready var player = $Player
onready var camera = $CameraHolder/Camera


func spawn_effect(effect_name, pos):
	$EffectSpawner.spawn_effect(effect_name, pos)


func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed():
		var mouse_coll_result = get_mouse_collision_pos(event.position)
		if mouse_coll_result:
			var action_pos = mouse_coll_result.position
			var int_object = mouse_coll_result.collider
			if int_object.is_in_group("interactable"):
				action_pos = mouse_coll_result.collider.get_target_position()
			$MousePosPointer.global_transform.origin = action_pos
			$MousePosPointer/AnimationPlayer.stop()
			if int_object.is_in_group("pickable"):
				player.walk_and_pickup_object(int_object)
				$MousePosPointer/AnimationPlayer.play("fade_item")
			else:
				player.walk_to_point(action_pos)
				$MousePosPointer/AnimationPlayer.play("fade")

	elif event is InputEventMouseMotion:
		var info_data = {}
		var mouse_coll_result = get_mouse_collision_pos(event.position)
		if mouse_coll_result:
			var int_object = mouse_coll_result.collider
			if int_object.is_in_group("interactable"):
				interactable_data_view(int_object.get_stats())
			elif int_object.is_in_group("pickable"):
				interactable_data_view(int_object.get_stats())
			else:
				interactable_data_view({})


func interactable_data_view(data: Dictionary):
	var info_text = ""
	for k in data.keys():
		info_text += "[" + k + ': ' + str(data[k]) + "] "
	$UI/Info.text = info_text


func get_player():
	return player


func get_mouse_collision_pos(mouse_pos):
	var from = camera.project_ray_origin(mouse_pos)
	var to = from + camera.project_ray_normal(mouse_pos) * 100
	var space_state = get_world().get_direct_space_state()
	var results = space_state.intersect_ray(from, to)
	if results.has("position"):
		return results
	return null

func update_item_view(money, items):
	var item_view_str = "money: " + str(money)  + "  "
	for k in items.keys():
		item_view_str += "[" + k + "]: " + str(items[k]) + " "
	$UI/ItemView.text = item_view_str

func _process(delta):
	$UI/FPS.set_text(str(Engine.get_frames_per_second()))

