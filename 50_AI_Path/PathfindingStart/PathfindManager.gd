extends Node

var queue = []
var cache = {}

var paths_calc_per_turn = 1


func _physics_process(delta):
	for i in range(paths_calc_per_turn):
		dequeue_path_request()


func dequeue_path_request():
	if queue.size() == 0:
		return
	var calc_path_info = queue.pop_front()
	var agent: MoveAgent = calc_path_info.agent
	var nav: Navigation = calc_path_info.nav
	var start_pos = agent.global_transform.origin
	var end_pos = agent.get_target_move_pos()
	var new_path = nav.get_simple_path(start_pos, end_pos)
	cache.erase(str(agent))
	agent.update_path(new_path)
	

func calc_path(agent: MoveAgent, nav: Navigation):
	var key = str(agent)
	if key in cache:
		return
	cache[key] = ""
	queue.append({
		"agent": agent, 
		"nav": nav
	})
	
