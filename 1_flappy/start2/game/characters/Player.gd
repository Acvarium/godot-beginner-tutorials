extends RigidBody2D
var started = false
const FLAP_FORCE = -300

func _physics_process(delta):
	if Input.is_action_just_pressed("flap"):
		start()
		flap()

func start():
	if started:
	    return
	started = true
	gravity_scale = 8
	$AnimationPlayer.play("flap")
	sleeping = false

func flap():
	linear_velocity.y = FLAP_FORCE
