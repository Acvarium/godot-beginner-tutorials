extends RigidBody2D
var started = false
var FLAP_FORCE = -300

func _physics_process(delta):
	if Input.is_action_just_pressed("flap"):
		start()
		flap()


func flap():
	linear_velocity.y = FLAP_FORCE


func start():
	if started:
		return
	started = true
	$AnimationPlayer.play("flap")
	gravity_scale = 8
	sleeping = false


func die():
	print("ddddddddddddd")
