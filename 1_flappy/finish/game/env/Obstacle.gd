extends Node2D
#Змінна яка посилається на головний вузол сцени
onready var main_node = get_tree().get_root().get_child(0)
var speed = 200 #змінна яка визначає швидкість руху труб

#Функція, яка виконується перед кожним перемалюванням екрану
func _process(delta):
	position.x += -speed * delta
	if global_position.x <= -200:
		queue_free()

#Опрацювання сигналу зіткнення гравця з трубою
func _on_Wall_body_entered(body):
	if body.name == "Player":
		if body.has_method("die"):
			body.die()
			

#Опрацювання сигналу виходу гравця з поля зарахування рахунку
func _on_Score_body_exited(body):
	if body.name == "Player":
		main_node.add_score()

#Функція, що зупиняє рух труб
func stop():
	speed = 0

