extends Node

var SAVE_PATH = "res://savegame.save"
var highscore = 0

func _ready():
	load_game()
	
	
func set_highscore(score):
	if score > highscore:
		highscore = score
		save_game()


func save_game():
	var save_dict = {}
	save_dict.hi = highscore
	var save_file = File.new()
	save_file.open(SAVE_PATH, File.WRITE) 
	save_file.store_line(to_json(save_dict))
	save_file.close()


func load_game():
	var save_file = File.new()
	if !save_file.file_exists(SAVE_PATH):
		return null
	save_file.open(SAVE_PATH, File.READ)
	var data = {}
	data = parse_json(save_file.get_as_text())
	highscore = data.hi
