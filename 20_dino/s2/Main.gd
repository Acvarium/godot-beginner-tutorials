extends Node2D

func _ready():
	update_score(0)
	update_highscore()
	
	
func game_over():
	$Slider.stopped = true
	$CanvasLayer/RestartButton.visible = true
	Global.set_highscore($Slider.get_score())
	update_highscore()

func _on_RestartButton_pressed():
	get_tree().reload_current_scene()

func update_highscore():
	$CanvasLayer/Highscore.text = str(Global.highscore)
	
func update_score(score):
	$CanvasLayer/Score.text = str(score).pad_zeros(5)
