extends Node2D
onready var main_node = get_tree().get_root().get_node("Main")

var speed = 1200
var ground_width = 2400
var stopped = false
var obstacle_files = [
	preload("res://Obstacles/cb0.tscn"),
	preload("res://Obstacles/cb1.tscn")
	]

var dist = 0.0

func _ready():
	randomize()


func get_score():
	return int(dist / 100)
	
func _physics_process(delta):
	if stopped:
		return
	
	dist += speed * delta
	main_node.update_score(int(dist / 100))
	for c in $GroundLayer.get_children():
		c.position.x -= speed * delta
		if c.global_position.x < -ground_width - 100:
			if c.is_in_group("ground"):
				c.global_position.x += ground_width * 2
			else:
				c.queue_free()

func add_obstacle():
	var obst_index = randi() % obstacle_files.size()
	var obstacle = obstacle_files[obst_index].instance()
	$GroundLayer.add_child(obstacle)
	var pos_x_offset = randf() * 300
	obstacle.global_position = $GenPos.global_position 
	obstacle.global_position.x += pos_x_offset

func _on_SpawnTimer_timeout():
	add_obstacle()
