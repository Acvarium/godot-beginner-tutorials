extends Node2D

func _ready():
	update_score(0)

func game_over():
	$Slider.stopped = true
	$CanvasLayer/ResetButton.visible = true
	$CanvasLayer/ResetButton.disabled = false
	Global.update_high_score($Slider.get_score())
	
func update_score(score):
	$CanvasLayer/Score.text = str(score).pad_zeros(6)
	$CanvasLayer/HiScore.text = "HI: " + str(Global.high_score).pad_zeros(6)

func _on_ResetButton_pressed():
	get_tree().reload_current_scene()
