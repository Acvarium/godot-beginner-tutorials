extends Node
var high_score = 0
var SAVE_PATH = "res://savegame.save"

func update_high_score(new_score):
	if new_score > high_score:
		high_score = new_score
		save_game()

func _ready():
	load_game()


func save_game():
	var save_dict = {}
	save_dict.hi = high_score
	var save_file = File.new()
	save_file.open(SAVE_PATH, File.WRITE) 
	save_file.store_line(to_json(save_dict))
	save_file.close()


func load_game():
	var save_file = File.new()
	if !save_file.file_exists(SAVE_PATH):
		return null
	save_file.open(SAVE_PATH, File.READ)
	var data = {}
	data = parse_json(save_file.get_as_text())
	high_score = data.hi
