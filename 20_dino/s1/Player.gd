extends KinematicBody2D

onready var main_node = get_tree().get_root().get_node("Main")
onready var gravity = 4000
var velocity = Vector2.ZERO
var jump_speed = -1200.0
var stopped = false

func _physics_process(delta):
	if stopped:
		return
	velocity.y += gravity * delta
	if is_on_floor() and Input.is_action_pressed("jump"):
		velocity.y = jump_speed
	velocity = move_and_slide(velocity, Vector2.UP)

func die():
	print("aaaaaaaaaa")
	main_node.game_over()
	stopped = true
	$AnimationPlayer.play("die")
