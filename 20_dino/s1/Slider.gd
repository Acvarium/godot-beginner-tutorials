extends Node2D

onready var main_node = get_tree().get_root().get_node("Main")
export(NodePath) var spawn_pointer_path
onready var spawn_pos = get_node(spawn_pointer_path).global_position

var dist = 0.0
var stopped = false

var speed = 800.0
var ground_width = 2400
var extra_offset = 100
var obstacle_files = [
	preload("res://obstacles/cb0.tscn"),
#	preload("res://obstacles/cb1.tscn"),
#	preload("res://obstacles/cb2.tscn"),
#	preload("res://obstacles/cb3.tscn"),
	preload("res://obstacles/cb4.tscn"),
	]

func get_score():
	return int(dist/100)

func _ready():
	randomize() 

func _physics_process(delta):
	if stopped:
		return
	for c in $GroundLayer.get_children():
		c.position.x -= speed * delta
		dist += speed * delta
		if c.global_position.x < -(ground_width + extra_offset):
			if c.is_in_group("ground"):
				c.global_position.x += ground_width * 2
			else:
				c.queue_free()
		main_node.update_score(int(dist/100))


func add_obstacle():
	var obst_index = randi() % obstacle_files.size()
	var obstacle = obstacle_files[obst_index].instance()
	$GroundLayer.add_child(obstacle)
	var rand_shift = randf() * 500
	obstacle.global_position = spawn_pos
	obstacle.global_position.x += rand_shift


func _on_SpawnTimer_timeout():
	add_obstacle()
