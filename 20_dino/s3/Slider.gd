extends Node2D
onready var main_node = get_tree().get_root().get_node("Main")


var speed = 1000
var ground_width = 2400
var dist = 0.0



var obstacle_files = [
	preload("res://Obstacles/Obstacle.tscn"),
	preload("res://Obstacles/CB1.tscn"),
	]

func get_score():
	return int(dist / 100)
	
func _ready():
	randomize()


func _physics_process(delta):
	dist += speed * delta
	main_node.update_score(int(dist / 100))
	for c in $GroundLayer.get_children():
		c.position.x -= speed * delta
		if c.global_position.x < -(ground_width + 100):
			if c.is_in_group("ground"):
				c.global_position.x += ground_width * 2
			else:
				c.queue_free()

func add_obstacle():
	var obst_index = randi() % obstacle_files.size()
	
	var obstacle = obstacle_files[obst_index].instance()
	$GroundLayer.add_child(obstacle)
	obstacle.global_position = $SpawnPosition.global_position
	

func _on_ObstSpawnTimer_timeout():
	add_obstacle()
