extends Node2D

func game_over():
	Global.set_highscore($Slider.get_score())
	$Slider.speed = 0
	$CanvasLayer/RestartButton.visible = true
	update_highscore()

func _ready():
	update_score(0)
	update_highscore()

func _on_RestartButton_pressed():
	get_tree().reload_current_scene()

func update_highscore():
	$CanvasLayer/Highscore.text = str(Global.highscore).pad_zeros(5)
	
	
func update_score(score):
	$CanvasLayer/Score.text = str(score).pad_zeros(5)
