extends KinematicBody2D
onready var main_node = get_tree().get_root().get_node("Main")

var gravity = 4000
var velocity = Vector2()
var jump_speed = -1200
var alive = true

func _physics_process(delta):
	if not alive:
		return
	velocity.y += gravity * delta

	if is_on_floor() and Input.is_action_pressed("jump"):
		velocity.y = jump_speed
	velocity = move_and_slide(velocity, Vector2.UP)


func die():
	$AnimationPlayer.play("die")
	alive = false
	main_node.game_over()
