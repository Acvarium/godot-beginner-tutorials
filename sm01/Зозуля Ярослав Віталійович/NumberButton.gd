extends Button

onready var main_node = get_tree().get_root().get_node("Main")
export var value = 1

func _ready():
	text = str(value)

func _on_NumberButton_pressed():
	main_node.number_clicked(value)
