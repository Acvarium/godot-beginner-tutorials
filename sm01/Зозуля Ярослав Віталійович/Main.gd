extends Node2D
var can_click
var row = 9
var col = 9
var size = 40
var Panel_scene = preload("res://Panel.tscn")
var arr = []
var cell_selected = Vector2()


func _ready():
	randomize()
	init_grids()
	init_arr()
	fill_numbers()
	clean_numbers()
	show()
	
func init_grids():
	var offset_x = 0
	var offset_y = 0
	for i in row:
		offset_x = 0
		offset_y += 2
		if i % 3 == 0:
			offset_y += 3
		for g in col:
			offset_x += 2
			if g % 3 == 0:
				offset_x += 3
			var panel = Panel_scene.instance()
			panel.rect_position = Vector2(size * g, size * i) + Vector2(offset_x, offset_y)
			panel.cell_pos = Vector2(i, g)
			$Cells.add_child(panel)

func init_arr():
	for i in row:
		for g in col:
			arr.append(null)

func show():
	var panels = $Cells.get_children()
	for i in row:
		for g in col:
			var number = arr[i * row + g]
			if number:
				panels[i * row + g].set_value(number)
				panels[i * row + g].set_locked(true)

func fill_numbers():
	for n in 3:
		var tmp = [1, 2, 3, 4, 5, 6, 7, 8, 9]
		tmp.shuffle()
		for i in range(n * 3, n * 3 + 3):
			for g in range(n * 3, n * 3 + 3):
				arr[i * row + g] = tmp.pop_back()
	fill_empty()

func fill_empty():
	var find_null = arr.find(null)
	if find_null == -1:
		return true
	var i = find_null / row
	var g = find_null % col
	var numbs = get_numb(i, g)
	if !numbs:
		return false
	var fill
	while numbs:
		fill = numbs.pop_back()
		arr[i * row + g] = fill
		if fill_empty():
			return true
		else:
			arr[i * row + g] = null

func get_numb(i, g):
	var tmp = [1, 2, 3, 4, 5, 6, 7, 8, 9]
	for number in get_row_numbers(i):
		tmp.erase(number)
	for number in get_col_numbers(g):
		tmp.erase(number)
	for number in get_area_numbers(i, g):
		tmp.erase(number)
	return tmp

func get_row_numbers(i):
	var res = []
	for g in col:
		var number = arr[i * row + g]
		if number:
			res.append(number)
	return res

func get_col_numbers(g):
	var res = []
	for i in row:
		var number = arr[i * row + g]
		if number:
			res.append(number)
	return res

func get_area_numbers(i, g):
	var offset_i = i / 3 * 3
	var offset_g = g / 3 * 3
	var res = []
	for area_i in range(offset_i, offset_i + 3):
		for area_g in range(offset_g, offset_g + 3):
			var number = arr[area_i * row + area_g]
			if number:
				res.append(number)
	return res

func clean_numbers():
	for i in row:
		for g in col:
			var numbs = get_numb(i, g)
			if numbs.empty():
				arr[i * row + g] = null

func number_clicked(value):
	$butt/Control.visible = false
	var panels = $Cells.get_children()
	panels[cell_selected.x * row + cell_selected.y].set_value(value)
	

func cell_clicked(cell_pos):
	cell_selected = cell_pos
	$butt/Control.visible = true


func check_numbers():
	pass
