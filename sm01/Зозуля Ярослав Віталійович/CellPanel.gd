extends Panel
onready var main_node = get_tree().get_root().get_node("Main")
var is_locked = false
var cell_pos = Vector2()
var cell_value = -1

func _on_CellButton_pressed():
	if !is_locked:
		main_node.cell_clicked(cell_pos)


func set_locked(value):
	is_locked = value
	if is_locked:
		self_modulate = Color(0.9, 0.9, 0.9)

func set_value(value):
	cell_value = value
	if cell_value > 0:
		$Label.text = str(cell_value)
