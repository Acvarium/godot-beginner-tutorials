extends Node2D

var piece_loaded = preload("res://pieces/Piece.tscn")
const PIECE_IMAGE_SIZE = 80.0
const LEFT_OFFSET = 10.0

var width = 4
var height = 4
var offset = 1.0
var piece_scale = 1.0
var x_start = 1.0
var y_start = 1.0
var all_pieces = [ ]
var number_of_pieces = 6
var touch_on_grid = Vector2()
var controlling = false


func make_2d_array():
	var array = []
	for i in width:
		array.append([])
		for j in height:
			array[i].append(null)
	return array


func _ready():
	randomize()
	all_pieces = make_2d_array()
	var screen_size = get_viewport().get_visible_rect().size
	offset = (screen_size.x - LEFT_OFFSET * 2) / width
	piece_scale = offset / PIECE_IMAGE_SIZE
	x_start = offset / 2 + LEFT_OFFSET
	y_start = screen_size.y - offset / 2 - (screen_size.y - (offset * height)) / 2
	spawn_pieces()


func grid_to_pixel(col, row):
	var new_x = x_start + offset * col
	var new_y = y_start + -offset * row
	return Vector2(new_x, new_y)
	
func pixel_to_grid(pixel_pos):
	var new_col = round((pixel_pos.x - x_start) / offset)
	var new_row = round((pixel_pos.y - y_start) / -offset)
	return Vector2(new_col, new_row)


func spawn_pieces():
	var index = 0
	var d = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	for i in range(width):
		for j in range(height):
			var p = randi() % d.size()
			random_piece(i, j, d[p])
			d.remove(p)


func random_piece(i, j, index):
	var rand = index
	var piece = piece_loaded.instance()
	add_child(piece)
	piece.position = grid_to_pixel(i, j)
	piece.color_index = rand
	piece.get_node("Sprite").scale = Vector2(piece_scale, piece_scale)
	piece.get_node("Sprite").frame = rand
	all_pieces[i][j] = piece
	
func is_in_grid(grid_pos):
	return grid_pos.x >= 0 and grid_pos.x < width and grid_pos.y >= 0 and grid_pos.y < height
	
func swap_pieces(grid_pos1, grid_pos2):
	var first_piece = all_pieces[grid_pos1.x][grid_pos1.y]
	var other_piece = all_pieces[grid_pos2.x][grid_pos2.y]
	if first_piece == null or other_piece == null:
		return
	all_pieces[grid_pos1.x][grid_pos1.y] = other_piece
	all_pieces[grid_pos2.x][grid_pos2.y] = first_piece
	var first_pos = first_piece.position
	first_piece.position = other_piece.position
	other_piece.position = first_pos

func _input(event):
	if event is InputEventScreenTouch and event.index == 0:
		if event.is_pressed():
			var first_touch = event.position
			touch_on_grid = pixel_to_grid(first_touch)
			if is_in_grid(touch_on_grid):
				controlling = true
		else:
			if !controlling:
				return
			var final_touch = event.position
			if is_in_grid(touch_on_grid):
				var empty_cell = null
				if touch_on_grid.x -1 >= 0:
					if all_pieces[touch_on_grid.x -1][touch_on_grid.y].color_index == 15:
						 empty_cell = Vector2(touch_on_grid.x -1, touch_on_grid.y)
				if touch_on_grid.x + 1 < width:
					if all_pieces[touch_on_grid.x +1][touch_on_grid.y].color_index == 15:
						 empty_cell = Vector2(touch_on_grid.x +1, touch_on_grid.y)
				if touch_on_grid.y -1 >= 0:
					if all_pieces[touch_on_grid.x][touch_on_grid.y -1].color_index == 15:
						 empty_cell = Vector2(touch_on_grid.x, touch_on_grid.y -1)
				if touch_on_grid.y + 1 < height:
					if all_pieces[touch_on_grid.x][touch_on_grid.y  +1].color_index == 15:
						 empty_cell = Vector2(touch_on_grid.x, touch_on_grid.y +1)
				if empty_cell != null:
					swap_pieces(touch_on_grid, empty_cell) 
