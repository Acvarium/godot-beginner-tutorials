extends Node2D

var can_control = true

func _on_Control_gui_input(event):
	if event is InputEventMouseButton and can_control:
		if event.is_action_pressed("click"):
			$AnimationPlayer.play("turn_over")


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "turn_over":
		get_parent().open_cards.append(self)
		get_parent().check()
