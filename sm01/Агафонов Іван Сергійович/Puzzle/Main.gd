extends Node2D

var Card = preload("res://Card.tscn")

var Textures = [
	preload("res://020.png"),
	preload("res://015.png"),
	preload("res://002.png"),
	preload("res://003.png"),
	preload("res://007.png"),
	preload("res://010.png"),
	preload("res://013.png"),
	preload("res://017.png"),
	preload("res://014.png"),
	preload("res://022.png"),
]

var cards = []
var open_cards = []
var ROW = 4
var COW = 5

func _ready():
	for t in Textures:
		var card = Card.instance()
		card.get_node("Obverse").texture = t
		cards.append(card)
		var card_match = Card.instance()
		card_match.get_node("Obverse").texture = t
		cards.append(card_match)
		
		cards.shuffle()
		
	for row in ROW:
		for col in COW:
			var c = cards[col + row * 5]
			c.position = Vector2(500, 100) + Vector2(140 * col, 140 * row)
			add_child(c)

func check():
	if open_cards.size() > 1:
		print(open_cards)
		if len(open_cards) >= 2:
			for card in cards:
				if is_instance_valid(card):
					card.can_control = false
				
		if open_cards[0].get_node("Obverse").texture == \
			open_cards[1].get_node("Obverse").texture:
			$DeleteTimer.start()
		else:
			$TurnBackTimer.start()

func cont_control():
	for card in cards:
		if is_instance_valid(card):
			card.can_control = true
	open_cards = []

func _on_Timer_timeout():
	for card in open_cards:
		card.queue_free()
	cont_control()

func _on_TurnBackTimer_timeout():
	for card in cards:
		if is_instance_valid(card):
			card.get_node("AnimationPlayer").play("turn_back")
	cont_control()
