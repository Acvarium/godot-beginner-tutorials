extends Node2D


var current_score
var high_score
  

signal set_score


func _ready():
	current_score = 0 
	emit_signal("set_score" , current_score)



func increase_score(amount: int):
	current_score += amount 
	emit_signal("set_score" , current_score)



func _on_Grid_change_score(amount: int):
	increase_score(amount)
