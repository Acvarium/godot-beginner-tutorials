extends Control


onready var  score_label = $CurrentScoreLabel
onready var game_over = $GameOver

# Called when the node enters the scene tree for the first time.
func _ready():
	game_over.hide()




func set_current_score(current_score: int ):
	score_label.text = String(current_score)


func _on_Grid_game_over():
	game_over.show()


func _on_Button_pressed():
	get_tree().reload_current_scene()


func _on_ScoreManager_set_score(current_score: int):
	set_current_score(current_score)
