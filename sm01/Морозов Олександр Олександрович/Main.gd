extends Node2D
var Card = preload("res://Card.tscn")
var Textures = [
	preload("res://Pictures/002.png"),
	preload("res://Pictures/003.png"),
	preload("res://Pictures/004.png"),
	preload("res://Pictures/005.png"),
	preload("res://Pictures/006.png"),
	preload("res://Pictures/007.png"),
	preload("res://Pictures/008.png"),
	preload("res://Pictures/009.png"),
	preload("res://Pictures/010.png"),
	preload("res://Pictures/011.png"),
	preload("res://Pictures/012.png"),
	preload("res://Pictures/013.png"),
	preload("res://Pictures/014.png"),
	preload("res://Pictures/015.png"),
	preload("res://Pictures/016.png"),
	preload("res://Pictures/017.png"),
	preload("res://Pictures/018.png"),
	preload("res://Pictures/019.png"),
	preload("res://Pictures/020.png"),
	preload("res://Pictures/021.png"),
	preload("res://Pictures/022.png"),
	preload("res://Pictures/023.png"),
	preload("res://Pictures/024.png"),
	]

var cards = []
var open_cards = []
var ROW = 4
var COL = 5


func _ready():
	print (open_cards)
	for t in Textures:
		var card = Card.instance()
		card.get_node("Obverse").texture = t
		cards.append(card)
		var card_match = Card.instance()
		card_match.get_node("Obverse").texture = t
		cards.append(card_match)
	
	cards.shuffle()
	
	for row in ROW:
		for col in COL:
			var c = cards[col + row * 5]
			c.position = Vector2(32,32) + Vector2(64 * col,64 * row)
			add_child(c)

func _on_Timer_timeout():
	for card in open_cards:
		card.queue_free()
		open_cards = []


func _on_TurnBackTimer_timeout():
	for card in open_cards:
		card.get_node("AnimationPlayer").play("turn_back")
	for card in cards:
		card.can_control = true
		open_cards = []
