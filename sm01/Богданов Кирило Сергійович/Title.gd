extends Node2D

var is_cover = true 
var flagged = false
var is_bomb = false
var tile 
const CELL_SIZE = 87

func get_surrounds():
	var surrounds = []
	var offsets = [
		(Vector2.UP + Vector2.LEFT) * CELL_SIZE,
		(Vector2.UP) * CELL_SIZE,
		(Vector2.UP + Vector2.RIGHT) * CELL_SIZE,
		Vector2.LEFT * CELL_SIZE,
		Vector2.RIGHT* CELL_SIZE,
		(Vector2.DOWN + Vector2.LEFT) * CELL_SIZE,
		(Vector2.DOWN) * CELL_SIZE,
		(Vector2.DOWN + Vector2.RIGHT) * CELL_SIZE,
	]
	for offset in offsets:
		for tiles in get_parent().tiles:
			if tiles.position == position + offset:
				surrounds.append(tiles)
	return surrounds
	

func set_bomb():
	is_bomb = true
	$bomb.show()
	$Label.hide()

func uncover():
	if flagged == false:
		$Dirt.hide()
		is_cover = false 
		var count_surrounds = 0
		for tile in get_surrounds():
			if  tile.is_bomb:
				count_surrounds +=1
		if count_surrounds > 0:
			$Label.text = str(count_surrounds)
		else:
			for tile in get_surrounds():
				if tile.is_cover:
					tile.uncover() 
	
		
func _on_Control_gui_input(event):
	if event is InputEventMouseButton:
		if event.is_action_pressed("left_click"):
			uncover()
	if event.is_action_pressed("rigt_click"):
		toggle_flag()

func toggle_flag():
	if is_cover:
		if flagged:
			flagged = false 
			$flag.hide()
		else:
			flagged = true
			$flag.show()

 
	
