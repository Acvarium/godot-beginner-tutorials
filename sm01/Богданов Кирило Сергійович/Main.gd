extends Node2D
var row = 8
var col = 8
var Tile = preload("res://Title.tscn")
var tiles
var rng = RandomNumberGenerator.new()
var bombs_number

func random_bomb(): 
	var bombs_number = randi()
	print(bombs_number)

func _ready():
	for r in row:
		for c in col:
			var t = Tile.instance()
			t.position = Vector2(r, c) * 87
			add_child(t)
		tiles = get_children()
		set_bombs()

func set_bombs():
	var n = 0
	if n != bombs_number:
		var tile = tiles[randi() % len(tiles)]
		if tile.is_bomb == false:
			tile.set_bomb()
			n += 1 
