extends Label

var second = 0
var minute = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Timer_timeout():
	second += 1
	if second == 60:
		minute +=1
		second = 0
	text = str(minute,":",second)
