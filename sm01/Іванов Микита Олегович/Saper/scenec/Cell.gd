extends Node2D

var bomb = false
var flag = false
var counter = 0
var open = false


func open():
	if !open:
		open = true
		$Area2D/Sprite/Timer.start()
		$Area2D/Sprite/Tween.interpolate_property($Area2D/Sprite, "scale", scale, Vector2(1.5, 1.5), 2, \
			Tween.TRANS_ELASTIC, Tween.EASE_OUT)
		$Area2D/Sprite/Tween.start()
		$Area2D/Sprite/Tween.interpolate_property($Area2D/Sprite, "scale", scale, Vector2(0.1, 0.1), 1, \
			Tween.TRANS_ELASTIC)
		$Area2D/Sprite/Tween.start()
		return counter


func set_cell_info(is_boomb = false, bobm_count = 0):
	counter = bobm_count
	if is_boomb:
		bomb = true
		$Area2D/bomb.visible = true
		$Area2D/Label.visible = false
	elif bobm_count > 0:
		$Area2D/bomb.visible = false
		$Area2D/Label.visible = true
		$Area2D/Label.text = str(bobm_count)


func get_cell_info():
	return bomb


func set_flag():
	if flag:
		$Area2D/flag.visible = false
		flag = false
	else:
		$Area2D/flag.visible = true
		flag = true
	if bomb and flag:
		print("правельно")
		return 1
	elif !bomb and !flag:
		return 0
	elif !bomb and flag:
		return 0
	elif bomb and !flag:
		return -1


func flag_info():
	return flag


func pusto():
	if counter == 0 and !bomb: return true
	else: return false


func _on_Timer_timeout():
	$Area2D/Sprite.visible = false
