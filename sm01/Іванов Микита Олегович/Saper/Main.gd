extends Node2D

var start = false

func game_over():
	print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
	$g1.visible = true
	$g2.visible = true
	$Label2/Timer.stop()
	$restart.start()
# Called when the node enters the scene tree for the first time.


func _input(event):
	if !start:
		start = true
		$Label2/Timer.start()


func win():
	$Label2/Timer.stop()
	$restart.start()
	$g1.text = "WIN"
	$g2.text = "WIN"
	$g1.visible = true
	$g2.visible = true


func _on_restart_timeout():
	get_tree().reload_current_scene()
