extends Node2D

var cell_load = preload("res://scenec/Cell.tscn")
onready var Main = get_tree().get_root().get_node("Main")
const CELL_SIZE = 48.0
const CELL_MARGIN = 24.0
const MAX_BOOMB = 20
var x_start = 24.0
var y_start = 24.0
var offset = 48
var cells = []
var width = 10
var height = 16
var bomb = 0
var def = 0
var flag = MAX_BOOMB
var in_game = true
var empty = []
var empty_last = []


func array_maker():
	var array = []
	for i in width:
		array.append([])
		for j in height:
			array[i].append(null)
	return array
	
func _ready():
	randomize()
	cells = array_maker()
	empty = array_maker()
	empty_last = array_maker()
	spawn_grid()
	bomb_generator()
	info_cell_generator()


func bomb_generator():
	var loop = 100
	while bomb < MAX_BOOMB and loop > 0:
		randomize()
		var rand_x = round(rand_range(0,width-1))
		var rand_y = round(rand_range(0,height-1))
		var rand_bomb = cells[rand_x][rand_y]
		if !rand_bomb.get_cell_info():
			rand_bomb.set_cell_info(true)
			bomb += 1
		else:
			loop -= 1

func info_cell_generator():
	for i in range(width):
		for j in range(height):
			if !cells[i][j].get_cell_info():
				var bomb_counter = 0
				for c in range(3):
					for v in range(3):
						if  i+1-c < 10 and j+1-v < 16 :
							print(i+1-c,",",j+1-v)
							if cells[i+1-c][j+1-v].get_cell_info():
								bomb_counter+=1
								print(bomb_counter)
				cells[i][j].set_cell_info(false,bomb_counter)

func spawn_grid():
	for i in range(width):
		for j in range(height):
			var cell = cell_load.instance()
			add_child(cell)
			cell.position = grid_to_pixel(i, j)
			cells[i][j] = cell

func grid_to_pixel(col, row):
	var new_x = x_start + offset * col
	var new_y = y_start + offset * row
	return Vector2(new_x, new_y)


func pixel_to_grid(pixel_pos):
	var new_col = round((pixel_pos.x - position.x - x_start) / offset)
	var new_row = round((pixel_pos.y - position.y - y_start) / offset)
	return Vector2(new_col, new_row)

func open(x,y):
	cells[x][y].open()
	var min_navkolo = 0
	if !cells[x][y].get_cell_info():
		for i in range(x - 1, x + 2):
			for j in range(y - 1, y + 2):
				if i >= 0 and i < width and j >= 0 and j < height:
					if cells[i][j].get_cell_info():
						min_navkolo += 1
		if min_navkolo == 0:
			for i in range(x - 1, x + 2):
				for j in range(y - 1, y + 2):
					if i >= 0 and i < width and j >= 0 and j < height:
						if !cells[i][j].open:
							open(i,j)


func _input(event):
	if Input.is_action_just_pressed("press") and in_game:
		if event is InputEventMouseButton:
			print(event.position)
			if event.position.x > 61 and event.position.y > 138 and event.position.x < 541 and event.position.y < 906:
				var click = event.position
				var grid_click = pixel_to_grid(click)
				print(grid_click)
				if grid_click.x < 10 and grid_click.y < 16:
					if !cells[grid_click.x][grid_click.y].flag_info():
						open(grid_click.x, grid_click.y)
#						var l_counter = cells[grid_click.x][grid_click.y].open()
#						if l_counter == 0 and !cells[grid_click.x][grid_click.y].get_cell_info():
#							print(l_counter)
#							empty_last[grid_click.x][grid_click.y] = cells[grid_click.x][grid_click.y]
##							empty_destroyer()
						if cells[grid_click.x][grid_click.y].get_cell_info():
							game_over()
	if Input.is_action_just_pressed("flag") and in_game:
		if event is InputEventMouseButton:
			print(event.position)
			if event.position.x > 61 and event.position.y > 138 and event.position.x < 541 and event.position.y > 906:
				var click = event.position
				var grid_click = pixel_to_grid(click)
				if grid_click.x < 10 and grid_click.y < 16:
					if cells[grid_click.x][grid_click.y].flag_info():
						flag += 1
						def += cells[grid_click.x][grid_click.y].set_flag()
						print(def)
					elif flag > 0:
						flag -= 1
						def += cells[grid_click.x][grid_click.y].set_flag()
						print(def)
					Main.get_node("Label").text = str(flag)
					if def == 20:
						win()
	
func game_over():
	in_game = false
	Main.game_over()

func win():
	Main.win()

#func empty_destroyer():
#	var ss = false
#	for l in range(20):
#		for i in range(width):
#			for j in range(height):
#				if empty_last[i][j] != null:
#					print("ezzz")
#					for c in range(1):
#						for v in range(1):
#							if  i+1-c < 10 and j+1-v < 16 :
#								pass
##								if cells[i+1-c][j+1-v].pusto() and cells[i+1-c][j+1-v] != empty_last[i][j]:
##									empty[i][j] = cells[i+1-c][j+1-v]
##									ss = true
##								cells[i+1-c][j+1-v].open()
#		empty_last = empty
#	empty = array_maker()
