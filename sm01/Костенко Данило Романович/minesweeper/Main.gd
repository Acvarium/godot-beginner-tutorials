extends Node2D

var TimeValue = 0
var row = 21
var col = 10
var bombs_number = 24
var Title = preload("res://Tile.tscn")
var tiles
onready var timer = get_node("Timer")


func _ready():
	timer.set_wait_time(2)
	timer.start()
	randomize()
	for r in row:
		for c in col:
			var t = Title.instance()
			t.position = Vector2(r, c) * 64
			$Tiles.add_child(t)
	tiles = $Tiles.get_children()
	set_bombs()

func set_bombs():
	var n = 0
	while n < bombs_number:
		var tile = tiles[randi() % len(tiles)]
		if tile.is_bomb == false:
			tile.set_bomb()
			n += 1


func _on_Restart_pressed():
	get_tree().change_scene("res://Main.tscn")




func _on_Timer_timeout():
	pass
