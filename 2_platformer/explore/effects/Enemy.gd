extends "res://actors/Player.gd"
export var current_dir = Vector2(1, 0)


func _ready():
	lives = 1


func check_inputs():
	attack_just_pressed = false
	if is_dead:
		return
	if $AttackCooldownTimer.is_stopped():
		var raycast = $PlayerDetectionRight
		if current_dir.x == -1:
			raycast = $PlayerDetectionLeft
		if raycast.is_colliding():
			var player = raycast.get_collider()
			if player.is_in_group("player"):
				if !player.is_hit():
					attack_just_pressed = true
					player.hit()


func hit(value = 1, hit_type = hit_types.TRAMPLE):
	if !is_dead:
		lives -= 1
		if lives <= 0:
			die(hit_type)

func die(hit_type):
	if hit_type == hit_types.TRAMPLE:
		$AnimationPlayer.play("die")
	elif hit_type == hit_types.FIREBALL:
		$AnimationPlayer.play("die_fireball")
	is_dead = true
	$DieTimer.start()


func get_direction():
	if current_dir.x > 0:
		if $WallDetectionRight.is_colliding():
			current_dir.x = -1
	elif current_dir.x < 0:
		if $WallDetectionLeft.is_colliding():
			current_dir.x = 1
	if is_dead:
		current_dir.x = 0
	return current_dir


func attack():
	if is_dead:
		return false
	if $AnimationPlayer.current_animation == "attack":
		return false
	$AttackCooldownTimer.start()
	return true


func _on_DieTimer_timeout():
	queue_free()
