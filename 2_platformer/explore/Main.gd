extends Node2D

func spawn_effect(effect_name, dir, pos):
	var effect_path = "res://effects/" + effect_name + ".tscn"
	var effect = load(effect_path)
	var effect_instance = effect.instance()
	$Effects.add_child(effect_instance)
	effect_instance.position = pos
	if effect_instance.has_method("set_direction"):
		effect_instance.set_direction(dir)
