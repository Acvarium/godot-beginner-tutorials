extends "res://Player.gd"

export var current_dir = Vector2(1, 0)



func get_direction():
	if current_dir.x > 0:
		if $WallDetectionRight.is_colliding():
			current_dir.x = -1
	elif current_dir.x < 0:
		if $WallDetectionLeft.is_colliding():
			current_dir.x = 1
	if is_dead:
		current_dir.x = 0
	return current_dir


func _on_DieTimer_timeout():
	queue_free()


func die():
	is_dead = true
	$DieTimer.start()


func hit(value = 1):
	if !is_dead:
		lives -= value
		if lives <= 0:
			die()

func check_inputs():
	attack_just_pressed = false
	if is_dead:
		return
	if $AttackCooldownTimer.is_stopped():
		var raycast = $PlayerDetectionRight
		if current_dir.x == -1:
			raycast = $PlayerDetectionLeft
		if raycast.is_colliding():
			var player = raycast.get_collider()
			if player.is_in_group("player"):
				print(player.is_hit())
				if !player.is_hit():
					attack_just_pressed = true
					player.hit()
		
		
func _ready():
	lives = 2
	add_to_group("enemy")
	speed = Vector2(100.0, 350.0)


func attack():
	if is_dead:
		return false
	if $AnimationPlayer.current_animation == "attack":
		return false
	$AttackCooldownTimer.start()
	return true
