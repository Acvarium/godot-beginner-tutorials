extends KinematicBody2D

export var action_suffix = ""
export var speed = Vector2(150.0, 350.0)
export var run_speed_mult = 2.0
export var anim_speed = 1.0
onready var gravity = ProjectSettings.get("physics/2d/default_gravity")
onready var main_node = get_tree().get_root().get_node("Main")
var _velocity = Vector2.ZERO

const FLOOR_DETECT_DISTANCE = 20.0

onready var sprite_base_scale = $Sprite.scale

var jump_just_pressed = false
var jump_just_released = false
var run_pressed = false
var attack_just_pressed = false
var is_dead = false
var lives = 4

enum anim_status {STAND, WALK, RUN, JUMP, ATTACK, HIT, DEAD}
var anim_state = anim_status.STAND

var enemy_trampled = false
var to_jump_on_enemy = false

func check_inputs():
	if is_dead:
		return
	jump_just_pressed = Input.is_action_just_pressed("jump" + action_suffix)
	jump_just_released = Input.is_action_just_released("jump" + action_suffix)
	run_pressed = Input.is_action_pressed("run")
	attack_just_pressed = Input.is_action_just_pressed("attack" + action_suffix)
	if enemy_trampled:
		jump_just_pressed = true
		jump_just_released = true
		enemy_trampled = false


func get_direction():
	var x_dir = Input.get_action_strength("move_right" + action_suffix) - Input.get_action_strength("move_left" + action_suffix)
	var y_dir = 0
	if  (is_on_floor() or to_jump_on_enemy) and jump_just_pressed:
		y_dir = -1
	if is_dead:
		x_dir = 0
	to_jump_on_enemy = false
	return Vector2(x_dir, y_dir)


func calculate_move_velocity(
		_velocity,
		_direction,
		_speed,
		_is_jump_interrupted
	):

	_velocity
	_velocity.x = _speed.x * _direction.x
	if _direction.y != 0.0:
		_velocity.y = _speed.y * _direction.y
	if _is_jump_interrupted:
		_velocity.y *= 0.6
	return _velocity
	
	
func _physics_process(delta):
	check_inputs()
	_velocity.y += gravity * delta
	var direction = get_direction()

	var is_jump_interrupted = jump_just_released and _velocity.y < 0.0
	var current_speed = speed
	if run_pressed:
		current_speed = Vector2(speed.x * run_speed_mult, speed.y)
	_velocity = calculate_move_velocity(_velocity, direction, current_speed, is_jump_interrupted)

	var snap_vector = Vector2.ZERO
	if direction.y == 0.0:
		snap_vector = Vector2.DOWN * FLOOR_DETECT_DISTANCE
	var is_on_platform = $PlatformDetector.is_colliding()
	_velocity = move_and_slide_with_snap(
		_velocity, snap_vector, Vector2.UP, not is_on_platform, 4, 0.9, false
	)
	if direction.x != 0:
		
		anim_state = anim_status.WALK
		if run_pressed:
			anim_state = anim_status.RUN
			
		if direction.x > 0:
			$Sprite.scale.x = sprite_base_scale.x * 1
		else:
			$Sprite.scale.x = sprite_base_scale.x * -1
	else:
		anim_state = anim_status.STAND
	if !is_on_floor():
		anim_state = anim_status.JUMP
	if attack_just_pressed and $AnimationPlayer.current_animation != "attack":
		if attack():
			anim_state = anim_status.ATTACK
	update_animation()
	press_enemy_down()


func update_animation():
	if is_dead:
		$AnimationPlayer.play("die")
		return
	if $AnimationPlayer.current_animation == "attack":
		return
	if $AnimationPlayer.current_animation == "hit":
		return
	if anim_state == anim_status.ATTACK:
		$AnimationPlayer.play("attack")
	if anim_state == anim_status.RUN:
		$AnimationPlayer.play("run")
	elif anim_state == anim_status.WALK:
		$AnimationPlayer.play("walk")
	elif anim_state == anim_status.STAND:
		$AnimationPlayer.play("idle")
	elif anim_state == anim_status.JUMP:
		$AnimationPlayer.play("jump")


func attack():
	if $AnimationPlayer.current_animation == "attack":
		return false
	var dir = Vector2(1, 0)
	if $Sprite.scale.x < 0:
		dir.x = -1
	main_node.spawn_effect("FireBall", dir, $Sprite/GunPosition.global_position)
	return true


func hit(value = 0):
	lives -= 1
	if lives > 0:
		$HitTimeout.start()
		$HitPlayer.play("hit")
	else:
		is_dead = true
		$AnimationPlayer.play("die")


func is_hit():
	if is_dead:
		return true
	if $HitTimeout.is_stopped():
		return false
	return true


func get_enemy_ray_collisions():
	var rays = [$EnemyRay1, $EnemyRay2]
	var enemy_hit = []
	for r in rays:
		r.force_raycast_update()
		if  r.is_colliding():
			if r.get_collider().is_in_group("enemy") and \
				!r.get_collider().is_dead:
					if !enemy_hit.has(r.get_collider()):
						enemy_hit.append(r.get_collider())
	return enemy_hit
	
func press_enemy_down():
	if is_in_group("player") and _velocity.y > 0:
		var enemies_hit = get_enemy_ray_collisions()
		if enemies_hit.size() > 0:
			for e in enemies_hit:
				e.hit(4)
				enemy_trampled = true
				to_jump_on_enemy = true
