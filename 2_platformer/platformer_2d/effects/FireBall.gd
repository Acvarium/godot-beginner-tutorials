extends KinematicBody2D

var move_dir = 1
var move_speed = 300.0

func set_direction(dir):
	move_dir = dir.x

func _physics_process(delta):
	position.x += delta * move_speed * move_dir


func _on_Area2D_body_entered(body):
	var main_node = get_tree().get_root().get_node("Main")
	main_node.spawn_effect("FireExplosion", Vector2(), global_position)
	if body.is_in_group("enemy"):
		body.hit()
	queue_free()


func _on_Timer_timeout():
	queue_free()
