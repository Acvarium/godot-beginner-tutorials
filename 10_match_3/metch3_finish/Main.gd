extends Node2D

func update_score(score):
	$CanvasLayer/Score.text = str(score)


func _on_RestartButton_pressed():
	get_tree().reload_current_scene()
