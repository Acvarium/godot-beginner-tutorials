extends Node2D

const PIECE_IMAGE_SIZE = 200.0

const LEFT_OFFSET = 40.0
export (int) var width
export (int) var height
onready var main_node = get_tree().get_root().get_node("Main")

var x_start = 1.0
var y_start = 1.0
var offset = 40.0
var piece_scale = 1.0
var score = 0

var number_of_pieces = 5

var piece_loaded = preload("res://pieces/green_piece.tscn")
var lotok5x6_loaded = preload("res://sprites/lotok5x6.png")

var all_pieces = []

var first_touch_on_grid = Vector2()
var final_touch_on_grid = Vector2()
var controlling = false
var locked = false


func _ready():
	if height == 6 and width == 5:
		main_node.set_background_texture(lotok5x6_loaded)
	var screen_size = get_viewport().get_visible_rect().size
	offset = (screen_size.x - LEFT_OFFSET * 2) / width
	piece_scale = offset / PIECE_IMAGE_SIZE
	
	x_start = offset / 2 + LEFT_OFFSET
	y_start = screen_size.y - offset / 2 - (screen_size.y - \
		(offset * height)) / 2
	randomize()
	all_pieces = make_2d_array()
	spawn_pieces()
	main_node.show_number_of_good_moves(check_moves())


func make_2d_array():
	var array = []
	for i in width:
		array.append([])
		for j in height:
			array[i].append(null)
	return array


func spawn_pieces():
	for i in range(width):
		for j in range(height):
			random_piece(i, j)


func all_pieces_to_data():
	var pieces_IDs = []
	for i in range(width):
		var row_data = []
		for j in range(height):
			if all_pieces[i][j] != null:
				row_data.append(all_pieces[i][j].color_index)
			else:
				row_data.append(-1)
		pieces_IDs.append(row_data)
	return pieces_IDs
	
	
func check_moves():
	var pieces_IDs = all_pieces_to_data()
	var good_moves = 0
	for i in range(width - 1):
		for j in range(height - 1):
			if pieces_IDs[i][j] == -1:
				continue
			var p = pieces_IDs[i][j]
			pieces_IDs[i][j] = pieces_IDs[i + 1][j]
			pieces_IDs[i + 1][j] = p
			if find_matches_from_data(pieces_IDs) != null:
				good_moves += 1
			pieces_IDs[i + 1][j] = pieces_IDs[i][j]
			pieces_IDs[i][j] = p
			
			pieces_IDs[i][j] = pieces_IDs[i][j + 1]
			pieces_IDs[i][j + 1] = p
			if find_matches_from_data(pieces_IDs) != null:
				good_moves += 1
			pieces_IDs[i][j + 1] = pieces_IDs[i][j] 
			pieces_IDs[i][j] = p
	if good_moves == 0:
		$RestartTimer.start()
	return good_moves
			

func random_piece(i, j):
	var rand = randi() % number_of_pieces
	var loops = 100
	while(match_at(i, j, rand) and loops > 0):
		rand = randi() % number_of_pieces
		loops -= 1
	var piece = piece_loaded.instance()
	add_child(piece)
	piece.position = grid_to_pixel(i, j)
	piece.color_index = rand
	piece.get_node("Sprite").frame = rand
	piece.get_node("Sprite").scale = Vector2(piece_scale, piece_scale)
	piece.get_node("Halo").scale = Vector2(piece_scale, piece_scale)
	piece.get_node("Sprite").rotation = randf() * PI * 0.5
	piece.highlite(false)
	all_pieces[i][j] = piece


func match_at(i, j, color_index):
	if i > 1:
		if all_pieces[i-1][j] != null and all_pieces[i-2][j] != null:
			if all_pieces[i-1][j].color_index == color_index and \
			all_pieces[i-2][j].color_index == color_index:
				return true
	if j > 1:
		if all_pieces[i][j-1] != null and all_pieces[i][j-2] != null:
			if all_pieces[i][j-1].color_index == color_index and \
			all_pieces[i][j-2].color_index == color_index:
				return true
	return false


func grid_to_pixel(col, row):
	var new_x = x_start + offset * col
	var new_y = y_start + -offset * row
	return Vector2(new_x, new_y)


func pixel_to_grid(pixel_pos):
	var new_col = round((pixel_pos.x - x_start) / offset)
	var new_row = round((pixel_pos.y - y_start) / -offset)
	return Vector2(new_col, new_row)


func is_in_grid(grid_pos):
	return grid_pos.x >= 0 and grid_pos.x < width and \
		grid_pos.y >= 0 and grid_pos.y < height


func _input(event):
	if locked:
		return
	if event is InputEventScreenTouch and event.index == 0:
		if event.is_pressed():
			var first_touch = event.position
			first_touch_on_grid = pixel_to_grid(first_touch)
			if is_in_grid(first_touch_on_grid):
				controlling = true
				all_pieces[first_touch_on_grid.x][first_touch_on_grid.y].highlite(true)
		else:
			if !controlling:
				return
			reset_piece_color(first_touch_on_grid)
			var final_touch = event.position
			final_touch_on_grid = pixel_to_grid(final_touch)
			if is_in_grid(final_touch_on_grid):
				if is_swap_allowed(first_touch_on_grid, final_touch_on_grid):
					swap_pieces(first_touch_on_grid, final_touch_on_grid)
					if !find_matches():
						$SwapBackTimer.start()
						locked = true
			controlling = false


func is_swap_allowed(grid_pos1, grid_pos2):
	var h_dist = abs(grid_pos1.x - grid_pos2.x)
	var v_dist = abs(grid_pos1.y - grid_pos2.y)
	if h_dist == 1 and v_dist == 0:
		return true
	if v_dist == 1 and h_dist == 0:
		return true
	return false


func reset_piece_color(grid_pos):
	if !is_valide_piece(grid_pos):
		return
	var _piece = all_pieces[grid_pos.x][grid_pos.y]
	_piece.highlite(false)


func is_valide_piece(grid_pos):
	if grid_pos.x < 0 or grid_pos.x >= width:
		return false
	if grid_pos.y < 0 or grid_pos.y >= height:
		return false
	if all_pieces[grid_pos.x][grid_pos.y] == null:
		return false
	return true


func swap_pieces(grid_pos1, grid_pos2):
	if !is_valide_piece(grid_pos1) or !is_valide_piece(grid_pos2):
		return
	var first_piece = all_pieces[grid_pos1.x][grid_pos1.y]
	var other_piece = all_pieces[grid_pos2.x][grid_pos2.y]
	if first_piece == null or other_piece == null:
		return
	all_pieces[grid_pos1.x][grid_pos1.y] = other_piece
	all_pieces[grid_pos2.x][grid_pos2.y] = first_piece
	var first_pos = first_piece.position
	first_piece.move(other_piece.position)
	other_piece.move(first_pos)


func find_matches_from_data(data):
	var list_of_matches = []
	for i in range(1, width - 1):
		for j in range(height):
			var number_of_matches = 0
			if data[i][j] == -1:
				continue
			for k in range(-1, 2, 2):
				if data[i + k][j] == -1:
					break
				if data[i + k][j] == data[i][j]:
					number_of_matches += 1
			if number_of_matches >= 2:
				for k in range(-1, 2):
					list_of_matches.append([i + k,j])
	for i in range(width):
		for j in range(1, height - 1):
			if data[i][j] == -1:
				continue
			var number_of_matches = 0
			for k in range(-1, 2, 2):
				if data[i][j + k] == -1:
					break
				if data[i][j + k] == data[i][j]:
					number_of_matches += 1
			if number_of_matches >= 2:
				for k in range(-1, 2):
					list_of_matches.append([i,j + k])
	if list_of_matches.size() != 0:
		return list_of_matches
	return null


func find_matches():
	for i in range(width):
		for j in range(height):
			if all_pieces[i][j] != null:
				all_pieces[i][j].matched = false
				all_pieces[i][j].highlite(false)
	var pieces_IDs = all_pieces_to_data()
	var list_of_matches = find_matches_from_data(pieces_IDs)
	if list_of_matches != null:
		for m in list_of_matches:
			all_pieces[m[0]][m[1]].matched = true
			all_pieces[m[0]][m[1]].highlite(true)
		$DestriyTimer.start()
		locked = true
		return true
	return false


func destroy_matched():
	for i in range(width):
		for j in range(height):
			if all_pieces[i][j] != null:
				if all_pieces[i][j].matched:
					all_pieces[i][j].queue_free()
					all_pieces[i][j] = null
					score += 1
	main_node.update_score(score)


func collaps_columns():
	var has_empty_cells = false
	for i in range(width):
		for j in range(height):
			if all_pieces[i][j] == null:
				has_empty_cells = true
				for k in range(j + 1, height):
					if all_pieces[i][k] != null:
						all_pieces[i][k].move(grid_to_pixel(i, j), Tween.EASE_OUT)
						all_pieces[i][j] = all_pieces[i][k]
						all_pieces[i][k] = null
						break
	if !has_empty_cells:
		$UnlockTimer.start() 


func refill_columns():
	var has_empty_cells = false
	for i in range(width):
		for j in range(height):
			if all_pieces[i][j] == null:
				has_empty_cells = true
				random_piece(i, j)
	if has_empty_cells:
		if !find_matches():
			$UnlockTimer.start()

func _on_DestriyTimer_timeout():
	destroy_matched()
	$CollapsTimer.start()


func _on_CollapsTimer_timeout():
	collaps_columns()
	$RefillTimer.start()


func _on_RefillTimer_timeout():
	refill_columns()


func _on_UnlockTimer_timeout():
	main_node.show_number_of_good_moves(check_moves())
	locked = false


func _on_SwapBackTimer_timeout():
	swap_pieces(first_touch_on_grid, final_touch_on_grid)
	$UnlockTimer.start()


func _on_RestartButton_pressed():
	get_tree().reload_current_scene()


func _on_RestartTimer_timeout():
	get_tree().reload_current_scene()
