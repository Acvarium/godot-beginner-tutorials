extends Node2D

var piece_loaded = preload("res://Pieces/Piece.tscn")
const PIECE_IMAGE_SIZE = 200.0
const LEFT_OFFSET = 40.0

var width = 4
var height = 6
var offset = 1.0
var piece_scale = 1.0
var x_start = 1.0
var y_start = 1.0

var all_pieces = []
var number_of_pieces = 6

var first_touch_on_grid = Vector2()
var final_touch_on_grid = Vector2()
var controlling = false

func _ready():
	randomize()
	all_pieces = make_2d_array()
	var screen_size = get_viewport().get_visible_rect().size
	offset = (screen_size.x - LEFT_OFFSET * 2) / width
	piece_scale = offset / PIECE_IMAGE_SIZE
	x_start = offset / 2 + LEFT_OFFSET
	y_start = screen_size.y - offset / 2 - (screen_size.y - (offset * height)) / 2
	spawn_pieces()

func grid_to_pixel(col, row):
	var new_x = x_start + offset * col
	var new_y = y_start + -offset * row
	return Vector2(new_x, new_y)


func spawn_pieces():
	for i in range(width):
		for j in range(height):
			random_piece(i, j)


func random_piece(i, j):
	var loops = 100
	var rand = randi() % number_of_pieces
	while(match_at(i, j, rand) and loops > 0):
		rand = randi() % number_of_pieces
		loops -= 1
	var piece = piece_loaded.instance()
	add_child(piece)
	piece.position = grid_to_pixel(i, j)
	piece.color_index = rand
	piece.set_image_scale(piece_scale)
	piece.get_node("Sprite").frame = rand
	all_pieces[i][j] = piece
	piece.highlite(false)


func _input(event):
	if event is InputEventScreenTouch and event.index == 0:
		if event.is_pressed():
			var first_touch = event.position
			first_touch_on_grid = pixel_to_grid(first_touch)
			if is_in_grid(first_touch_on_grid):
			    controlling = true
		else:
			if !controlling:
				return
			var final_touch = event.position
			final_touch_on_grid = pixel_to_grid(final_touch)
			if is_in_grid(final_touch_on_grid):
				swap_pieces(first_touch_on_grid, final_touch_on_grid)
				find_matches()


func swap_pieces(grid_pos1, grid_pos2):
	var first_piece = all_pieces[grid_pos1.x][grid_pos1.y]
	var other_piece = all_pieces[grid_pos2.x][grid_pos2.y]
	if first_piece == null or other_piece == null:
		return
	all_pieces[grid_pos1.x][grid_pos1.y] = other_piece
	all_pieces[grid_pos2.x][grid_pos2.y] = first_piece
	var first_pos = first_piece.position
	first_piece.move(other_piece.position)
	other_piece.move(first_pos)


func is_in_grid(grid_pos):
	return grid_pos.x >= 0 and grid_pos.x < width and \
		grid_pos.y >= 0 and grid_pos.y < height


func pixel_to_grid(pixel_pos):
	var new_col = round((pixel_pos.x - x_start) / offset)
	var new_row = round((pixel_pos.y - y_start) / -offset)
	return Vector2(new_col, new_row)
	

func match_at(i, j, color_index):
	if i > 1:
		if all_pieces[i-1][j] != null and all_pieces[i-2][j] != null:
			if all_pieces[i-1][j].color_index == color_index and \
			all_pieces[i-2][j].color_index == color_index:
				return true
	if j > 1:
		if all_pieces[i][j-1] != null and all_pieces[i][j-2] != null:
			if all_pieces[i][j-1].color_index == color_index and \
			all_pieces[i][j-2].color_index == color_index:
				return true
	return false


func find_matches():
	var found_matches = false
	for i in range(width):
		for j in range(height):
			if all_pieces[i][j] != null:
				all_pieces[i][j].matched = false
				all_pieces[i][j].get_node("Sprite").modulate.a = 1
				
	for i in range(1, width - 1):
		for j in range(height):
			if !all_pieces[i - 1][j] or !all_pieces[i][j] or !all_pieces[i + 1][j]:
				continue
			if all_pieces[i - 1][j].color_index == all_pieces[i][j].color_index and \
				all_pieces[i + 1][j].color_index == all_pieces[i][j].color_index:
				found_matches = true
				for k in range(-1, 2):
					all_pieces[i + k][j].matched = true
					all_pieces[i + k][j].get_node("Sprite").modulate.a = 0.5
					
	for i in range(width):
		for j in range(1, height - 1):
			if !all_pieces[i][j - 1] or !all_pieces[i][j] or !all_pieces[i][j + 1]:
				continue
			if all_pieces[i][j - 1].color_index == all_pieces[i][j].color_index and \
				all_pieces[i][j + 1].color_index == all_pieces[i][j].color_index:
				found_matches = true
				for k in range(-1, 2):
					all_pieces[i][j + k].matched = true
					all_pieces[i ][j + k].get_node("Sprite").modulate.a = 0.5
	if found_matches:
		$DestriyTimer.start()


func collaps_columns():
	for i in range(width):
		for j in range(height):
			if all_pieces[i][j] == null:
				for k in range(j + 1, height):
					if all_pieces[i][k] != null:
						all_pieces[i][k].move(grid_to_pixel(i, j), Tween.EASE_OUT)
						all_pieces[i][j] = all_pieces[i][k]
						all_pieces[i][k] = null
						break


func destroy_matched():
	for i in range(width):
		for j in range(height):
			if all_pieces[i][j] != null:
				if all_pieces[i][j].matched:
					all_pieces[i][j].queue_free()
					all_pieces[i][j] = null
	$CollapsTimer.start()
	

func refill_columns():
	var has_empty_cells = false
	for i in range(width):
		for j in range(height):
			if all_pieces[i][j] == null:
				has_empty_cells = true
				random_piece(i, j)
	if has_empty_cells:
		find_matches()


func make_2d_array():
	var array = []
	for i in width:
		array.append([])
		for j in height:
			array[i].append(null)
	return array


func _on_DestriyTimer_timeout():
	destroy_matched()


func _on_CollapsTimer_timeout():
	collaps_columns()
	$RefillTimer.start()


func _on_RefillTimer_timeout():
	refill_columns()
