extends Area2D
var speed = 300.0
var up
var down


func _ready():
	randomize()
	for i in range(10):
		print(randf())
		
	up = name.to_lower() + "_up" 
	down = name.to_lower() + "_down"


func _process(delta):
	var input = Input.get_action_strength(down) - \
		Input.get_action_strength(up)
		
	position.y += input * delta * speed
	position.y = clamp(position.y, 50, get_viewport_rect().size.y - 50)


func _on_Left_area_entered(area):
	if area.name == "Ball":
		area.direction = Vector2(1, 0)


func _on_Right_area_entered(area):
	if area.name == "Ball":
		area.direction = Vector2(-1, 0)
