extends Area2D

const DEFAULT_SPEED = 200
var direction = Vector2.LEFT

onready var speed = DEFAULT_SPEED

func _ready():
	pass 

func _process(delta):
	speed += delta * 2
	position += speed * delta * direction
