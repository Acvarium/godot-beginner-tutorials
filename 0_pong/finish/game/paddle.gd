extends Area2D

const MOVE_SPEED = 200

var _ball_dir
var _up
var _down

func _ready():
	var n = name.to_lower()
	_up = n + "_up"
	_down = n + "_down"
	_ball_dir = -1 
	if n == "left":
		_ball_dir = 1 


func _process(delta):
	# Move up and down based on input.
	var input = Input.get_action_strength(_down) - Input.get_action_strength(_up)
	position.y = position.y + input * MOVE_SPEED * delta
	position.y = clamp(position.y, 32, get_viewport_rect().size.y - 32)
	

func _on_area_entered(area):
	if area.name == "Ball":
		# Assign new direction.
		area.direction = Vector2(_ball_dir, randf() * 2 - 1).normalized()
