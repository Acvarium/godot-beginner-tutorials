extends Node2D
var left_score = 0
var right_score = 0

func reset_score():
	left_score = 0
	right_score = 0
	show_score()

func show_score():
	print(left_score, ":", right_score)
	$Control/LeftScore.text = str(left_score)
	$Control/RightScore.text = str(right_score)


func goal(dir):
	if dir < 0:
		left_score += 1
	else:
		right_score += 1
	show_score()
	

func _ready():
	reset_score()

func _on_LeftWall_area_entered(area):
	if area.name == "Ball":
		goal(1)
		area.reset()

func _on_RightWall_area_entered(area):
	if area.name == "Ball":
		goal(-1)
		area.reset()
