extends Area
export var key_name = ""

func _on_DoorKey_body_entered(body):
	if body.has_method("pickup_key"):
		body.pickup_key(key_name)
		queue_free()
