extends Spatial

func _ready():
	$AnimationPlayer.get_animation("walk").set_loop(true)
	$AnimationPlayer.get_animation("idle").set_loop(true)
	$AnimationPlayer.get_animation("attack").set_loop(true)
