extends Spatial
export var door_open = false
export var key_needed = ""

func _ready():
	open_door(door_open)


func toggle_door(keys = []):
	return open_door(!door_open, keys)


func open_door(to_open, keys = []):
	var used_key = ""
	if to_open:
		var allowed = true
		if key_needed != "":
			allowed = keys.has(key_needed)
		if allowed:
			used_key = key_needed
			key_needed = ""
			$MeshInstance/LockMesh.visible = false
			$AnimationPlayer.play("open")
			door_open = true
	else:
		$AnimationPlayer.play_backwards("open")
		door_open = false
	return used_key
