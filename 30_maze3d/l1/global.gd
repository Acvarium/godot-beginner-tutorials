extends Node
var current_scene = null
var current_level = -1
const MAX_LEVEL = 2


func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() -1)


func goto_scene(path):
	call_deferred("_deferred_goto_scene",path)


func _deferred_goto_scene(path):
	current_scene.free()
	var s = ResourceLoader.load(path)
	current_scene = s.instance()
	get_tree().get_root().add_child(current_scene)
	get_tree().set_current_scene(current_scene)


func open_next_level():
	open_level(current_level + 1)


func open_level(level_id):
	if level_id <= MAX_LEVEL:
		current_level = level_id
		goto_scene("res://Main.tscn")
	else:
		goto_scene("res://MainMenu.tscn")
