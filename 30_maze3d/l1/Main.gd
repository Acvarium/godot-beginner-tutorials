extends Spatial
var mouse_captured = false
var mouse_over_ui = false


func update_inventory_list(keys):
	var inv_text = "Inventory: "
	for k in keys:
		inv_text += k + " "
	$CanvasLayer/InventoryList.text = inv_text

func _process(delta):
	$CanvasLayer/FPS.set_text(str(Engine.get_frames_per_second()))
func _ready():
	capture_mouse(true)
	if Global.current_level > 0:
		for c in $Level.get_children():
			c.free()
		var level_file_name = "res://levels/Level" + \
			str(Global.current_level).pad_zeros(2) + ".tscn"
		
		var level_file = load(level_file_name)
		var level = level_file.instance()
		$Level.add_child(level)


func capture_mouse(to_capture):
	if $CanvasLayer/VictoryScreen.visible:
		to_capture = false
	if to_capture:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	mouse_captured = to_capture


func _input(event):
	if event.is_action_pressed("mouse_capture") and !mouse_over_ui:
		capture_mouse(true)
	if event.is_action_pressed("mouse_release"):
		capture_mouse(false)


func _on_MenuButton_pressed():
	Global.goto_scene("res://MainMenu.tscn")


func _on_MenuButton_mouse_entered():
	mouse_over_ui = true
	

func _on_MenuButton_mouse_exited():
	mouse_over_ui = false


func _on_NextLevelButton_pressed():
	Global.open_next_level()


func level_complited():
	$CanvasLayer/VictoryScreen.visible = true
	capture_mouse(false)
