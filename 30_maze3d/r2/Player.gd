extends KinematicBody
var mouse_sens = 30.0
const MAX_SPEED = 5
const MAX_RUN_SPEED = 15
const ACCEL = 2.0
const DEACCEL = 6.0
const MAX_SLOPE_ANGLE = 60
var camera_angle = 0.0
var gravity = -9.8 * 3.0
var velocity = Vector3()
var jump_hight = 10.0
var mouse_change = Vector2()


func _input(event):
	if event is InputEventMouseMotion:
		mouse_change = event.relative


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func aim(delta):
	if mouse_change.length() > 0:
		$Head.rotate_y(deg2rad(-mouse_change.x * mouse_sens * delta))
		var change_y = -mouse_change.y * mouse_sens * delta
		
		if change_y + camera_angle < 90 and change_y + camera_angle > -90:
			$Head/Camera.rotate_x(deg2rad(change_y))
			camera_angle += change_y
		
		mouse_change = Vector2()


func _physics_process(delta):
	aim(delta)
	walk(delta)


func walk(delta):
	var direction = Vector3()
	var _aim = $Head.global_transform.basis
	if Input.is_action_pressed("ui_up"):
		direction -= _aim.z
	if Input.is_action_pressed("ui_down"):
		direction += _aim.z
	if Input.is_action_pressed("ui_left"):
		direction -= _aim.x
	if Input.is_action_pressed("ui_right"):
		direction += _aim.x
	direction.y = 0
	direction = direction.normalized()
	if is_on_floor():
		var n = $Tail.get_collision_normal()
		var floor_angle = rad2deg(acos(n.dot(Vector3.UP)))
		if floor_angle > MAX_SLOPE_ANGLE:
			velocity.y += gravity * delta
	else:
		velocity.y += gravity * delta
	var speed = MAX_SPEED
	if Input.is_action_pressed("run"):
		speed = MAX_RUN_SPEED
	var target = direction * speed
	var temp_velocity = velocity
	temp_velocity.y = 0
	var acceleration = ACCEL
	if direction.dot(temp_velocity.normalized()) <= 0:
		acceleration = DEACCEL
		
	temp_velocity = temp_velocity.linear_interpolate(target, acceleration * delta)
	
	velocity.x = temp_velocity.x
	velocity.z = temp_velocity.z
	if (is_on_floor() or $Tail.is_colliding()) and Input.is_action_just_pressed("jump"):
		velocity.y = jump_hight
	velocity = move_and_slide_with_snap(velocity, Vector3.UP,  Vector3.UP)
