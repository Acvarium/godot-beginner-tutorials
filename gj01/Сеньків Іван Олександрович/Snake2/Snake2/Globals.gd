extends Node

var food_scene = preload("res://Food.tscn")
var snake_scene = preload("res://Snake.tscn")

var gameOver_scenes = preload("res://GameOverOverlay.tscn")

onready var main = get_node("/root/main")
onready var snake = get_node("/root/main/Snake")
onready var highscore_label = get_node("/root/main/Label")

var grid_size = Vector2(20,15)
var cell_size = 32
var highscore = 0

func eat_food():
	if snake == null:
		snake = get_node("/root/main/Snake")
		main = get_node("/root/main")
		highscore_label = get_node("/root/main/Label")
	snake.add_bodyPart()
	snake.get_node("EatFoodSound").play()
	highscore += 1
	highscore_label.text = str(highscore).pad_zeros(2)
	place_food()

func place_food():
	var food = food_scene.instance()
	food.global_position = get_random_grid_pos()
	main.add_child(food)

func  get_random_grid_pos():
	var posible_positions = []
	for x in grid_size.x:
		for y in grid_size.y:
			posible_positions.append(Vector2(x*cell_size,y*cell_size))
	
	for segment in snake.segments:
		posible_positions.erase(segment.global_position)
		
		randomize()
		posible_positions.shuffle()
		var random_pos = posible_positions[0]
		return random_pos

func reset_game():
	snake = snake_scene.instance()
	main.add_child(snake)
	reset_highscore()
	main.get_node("AudioStreamPlayer2D").play()

func reset_highscore():
	highscore = 0
	highscore_label.text = "00"

func game_over():
	snake.queue_free()
	var OverLay = gameOver_scenes.instance()
	main.add_child(OverLay) 
	main.get_node("AudioStreamPlayer2D").stop()
	
var current_scene = null


func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() -1)
	


func goto_scene(path):
	call_deferred("_deferred_goto_scene",path)


func _deferred_goto_scene(path):
	current_scene.free()
	var s = ResourceLoader.load(path)
	current_scene = s.instance()
	get_tree().get_root().add_child(current_scene)
	get_tree().set_current_scene(current_scene)
