extends KinematicBody2D

onready var eff = preload("res://effects/effect_boom.tscn")

var velocity = Vector2()
var direction = 1
var dmg


func _ready():
	$Timer.start()


func _physics_process(delta):
	velocity.x = direction * 50000 * delta
	velocity.y = 0
	move_and_slide(velocity,Vector2.UP)
	if $coliders/RayCast2D.is_colliding() and !$coliders/RayCast2D.get_collider().is_in_group("player"):
		if $coliders/RayCast2D.get_collider().is_in_group("damaged"):
			$coliders/RayCast2D.get_collider().take_damage(dmg)
		var boom = eff.instance()
		get_parent().add_child(boom)
		boom.position = position
		queue_free()
	if $coliders/RayCast2D2.is_colliding() and !$coliders/RayCast2D2.get_collider().is_in_group("player"):
		var boom = eff.instance()
		get_parent().add_child(boom)
		boom.position = position
		if $coliders/RayCast2D2.get_collider().is_in_group("damaged"):
			$coliders/RayCast2D2.get_collider().take_damage(dmg)
		queue_free()
		



func _on_Timer_timeout():
	queue_free()
