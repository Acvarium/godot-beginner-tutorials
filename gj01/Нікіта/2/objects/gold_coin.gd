extends KinematicBody2D


var velocity = Vector2()
var vel_pos = Vector2()
var gravity = 1200
var type = "Gold"

func s_type(set_type = "ret"):
	if set_type == "ret":
		return type
	if type == "Gold":
		$Sprite.modulate.r8 = 215
		$Sprite.modulate.g8 = 202
		$Sprite.modulate.b8 = 0
		type = set_type
	if type == "Ether":
		$Sprite.modulate.r8 = 215
		$Sprite.modulate.g8 = 0
		$Sprite.modulate.b8 = 71
		type = set_type
	
	
func _ready():
	pass # Replace with function body.
func drop():
	randomize()
	velocity.y += -700
	velocity.x += rand_range(-100,100)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	velocity.y += delta * gravity
	if (
	$RayCast2D.is_colliding() and 
	$RayCast2D.get_collider().is_in_group("player")):
		if (abs($RayCast2D.get_collider().bag_pos().x - position.x) < 10 and
			abs($RayCast2D.get_collider().bag_pos().y - position.y) < 10):
			$RayCast2D.get_collider().coin_take(type)
			queue_free()
		velocity = Vector2(
		($RayCast2D.get_collider().bag_pos().x - position.x)*5,
		($RayCast2D.get_collider().bag_pos().y - position.y)*5)
	else:
		velocity.x *= 0.99
	velocity = move_and_slide(velocity,Vector2.UP)


