extends Node2D

export var gold = 10
export var ether = 2
export var crystals = 0

onready var coin = preload("res://objects/coin.tscn")
onready var boom = preload("res://effects/effect_chest.tscn")
onready var main = get_tree().get_root().get_node("Main")
var loot = {"coin":10,"м'ячик":1,"шоколадка":1}
var open = false


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func open():
	if open:
		return
	open = true
	$Sprite.frame = 1
	var eff = boom.instance()
	main.get_node("effects").add_child(eff)
	eff.global_position = position
	eff.global_position.y = position.y - 70
	eff.scale = Vector2(2,2)
	for g in range(gold):
		var coins = coin.instance()
		main.get_node("objects").add_child(coins)
		coins.s_type("Gold")
		coins.global_position = position
		coins.global_position.y = position.y - 70
		coins.scale = Vector2(0.6,0.6)
		coins.drop()
		print(coins.global_position)
	for e in range(ether):
		var coins = coin.instance()
		main.get_node("objects").add_child(coins)
		coins.s_type("Ether")
		coins.global_position = position
		coins.global_position.y = position.y - 70
		coins.scale = Vector2(0.6,0.6)
		coins.drop()
		print(coins.global_position)
	for c in range(crystals):
		var coins = coin.instance()
		main.get_node("objects").add_child(coins)
		coins.s_type("crystals")
		coins.global_position = position
		coins.global_position.y = position.y - 70
		coins.scale = Vector2(0.6,0.6)
		coins.drop()
		print(coins.global_position)
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		open()
