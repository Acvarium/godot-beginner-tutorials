extends NinePatchRect

var enable = visible


func press():
	rect_position.x = rect_position.x + rect_size.x * rect_scale.x
	rect_scale.x = rect_scale.x * -1
	$Label.rect_scale.x = $Label.rect_scale.x * -1
	$Label.rect_position.x = ($Label.rect_position.x - 
	$Label.rect_size.x * 
	$Label.rect_scale.x)
	
	
func focus(entered):
	enable = visible
	if entered:modulate = "ffff"
	else: modulate = "c0c0c0"
	

func _on_Button_mouse_entered():
	focus(true)


func _on_Button_mouse_exited():
	focus(false)


func _on_Button_button_down():
	press()


func _on_Button_button_up():
	press()
