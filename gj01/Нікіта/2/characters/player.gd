extends KinematicBody2D

# Зовнішні змінні
export var speed = 15000
export var jump_force = 100
export var health = 100.0
export var max_health = 100.0
export var stamina = 100.0
export var max_stamina = 100.0
export var gold = 0
export var ether = 0
export var crystals = 0
export var max_jumps = 3
export (NodePath) var img

# Внутрішні змінні
onready var main = get_tree().get_root().get_node("Main")

var max_speed = speed
var gravity = 1200
var velocity = Vector2()
var in_damage = false # Стан отримання пошкождженнь
var jumps = 0
var is_not_noticed = true
var in_jump = 0
var move = 0 # Напрямок руху
var dash = false
var right_or_left = 1
var end_fl_collide = false # Знаходження гравця на нижньому кордоні рівня
onready var ray_cast_bottom = $RayCast
onready var ray_cast_left = $RayCastLeft
onready var ray_cast_right = $RayCastRight
var can_move = true # Можливість руху гравця
var coin_effect = preload("res://effects/effect_coin.tscn")
var spell = preload("res://effects/Spell.tscn")
var lvl = 1
var xp = 0
var xp_need = 100
var in_run = false
var current_animation = "idle"


var on_cooldown = false
var weapon = {"Type":"Spell","Damage":10,"Cooldown":1,"Stamina":10}
var dmg_scale = {"All":1,"Sword":1,"Spell":1,"Melee":1}


var in_dial = false
var dial_size = 0
var dial_speed = 20
var dial_stage = 0
var dial_max_stage = 0
var dial_now
var paused = false
var test_dial = [["Jame","Rom"],
["Привіт івффів фв фі вів  апаваіваі і ваіваіва","Ти івівфів  фі ві  фіввкуц   уваф  фв іф"]]


func stats_update():
	$CanvasLayer/StatsPanel/HpBar.max_value = max_health
	$CanvasLayer/StatsPanel/StaminaBar.max_value = max_stamina
	$CanvasLayer/StatsPanel/HpBar.value = health
	$CanvasLayer/StatsPanel/StaminaBar.value = stamina
	$CanvasLayer/StatsPanel/XpBar.value = xp
	$CanvasLayer/StatsPanel/XpBar.max_value = xp_need
	$CanvasLayer/StatsPanel/Lvl.text = str("LVL: ",lvl)
	$CanvasLayer/StatsPanel/Gold.text = str("Gold: ",int_to_text(gold))
	$CanvasLayer/StatsPanel/Ether.text = str("Ether: ",int_to_text(ether))
	$CanvasLayer/StatsPanel/Crystals.text = str("Crystals: ",int_to_text(crystals))


func anim_update():
	if move == 0 and is_on_floor() and current_animation != "idle" and !dash:
		$AnimationPlayer.play("idle")
		current_animation = "idle"
	if move != 0 and is_on_floor() and current_animation != "walk" and !dash:
		$AnimationPlayer.play("walk")
		current_animation = "walk"

	elif !is_on_floor() and current_animation != "fall" and !dash and !$on_floor.is_colliding():
		$AnimationPlayer.play("jump")
		current_animation = "fall"
	if in_run and current_animation == "walk":
		$AnimationPlayer.playback_speed = 1.5
	else:
		$AnimationPlayer.playback_speed = 1

func int_to_text(count):
	if count < 1000: return str(count)
	if count >= 1000000:
		if !round(count%1000000/100000):
			return str(round(count/1000000),"M")
		else:
			return str(round(count/1000000),".",round(count%1000000/100000),"M")
	elif count >= 1000: 
		if !round(count%1000/100):
			return str(round(count/1000),"K")
		else:
			return str(round(count/1000),".",round(count%1000/100),"K")

func movement(delta): # Функція перевірки подій та руху
	# Перевірка руху й зміна напрямку
	if can_move:
		if move == 1:
			if $sprite.transform.get_scale().y == -1:
				$sprite.apply_scale(Vector2(-1,1))
				right_or_left = 1
		if move == -1:
			if $sprite.transform.get_scale().y == 1:
				$sprite.apply_scale(Vector2(-1,1))
				right_or_left = -1
	
	# Переаврка подій руху
	if Input.is_action_just_pressed("Right"):
		move += 1
		print($sprite.transform.get_scale())
	if Input.is_action_just_pressed("Left"):
		move -= 1
		print($sprite.transform.get_scale())
	if Input.is_action_just_released("Right"):
		move -= 1
	if Input.is_action_just_released("Left"):
		move += 1
	if !Input.is_action_pressed("Right") and !Input.is_action_pressed("Left"):
		move = 0
	
	# Первічна налаштування руху
	velocity.x = max_speed * delta * move
	
	# Гравітація
	if !is_on_floor():
		velocity.y += gravity * delta
	
	# Перевірка знаходження на землі
	elif is_on_floor():
		jumps = max_jumps

	# Подія біга
	if Input.is_action_pressed("run"):
		max_speed = float(speed) * 2
		in_run = true
	if Input.is_action_just_released("run"):
		max_speed = speed
		in_run = false

	# Подія стрибка
	if Input.is_action_pressed("jump") and is_on_floor():
		print("jump!")
		$AnimationPlayer.play("jump_start")
		velocity.y = -jump_force * (gravity/4) * delta
		jumps -= 1
	elif Input.is_action_just_pressed("jump") and jumps > 0:
		print("jump!")
		$AnimationPlayer.play("jump_start")
		velocity.y = -jump_force * (gravity/4) * delta 
		jumps -= 1

	# Подія випаду
	if  Input.is_action_just_pressed("dash") and stamina >= 10:
			dash = true
			$AnimationPlayer.play("dash")
			current_animation = "dash"
			stamina -= 10
			$DashTime.start(float(speed)/150000)
	if dash:
		velocity.x += speed*delta*10*right_or_left

	# Подія стелс позиції
	if !in_dial:can_move = true
	if Input.is_action_pressed("down") and is_on_floor() and !in_dial:
		if !end_fl_collide: # Заборона заглядати за кордони рівня
			$Camera2D.limit_bottom = 10000000
		can_move = false
	if can_move:
		if velocity.x != 0 and in_run and !dash:
			stamina -= 0.1
		velocity = move_and_slide(velocity, Vector2.UP)

func camera_limiter(): # Функція встановлення лімітів камери
	end_fl_collide = false
	ray_cast_bottom.force_raycast_update()
	
	# Перевірка поверхні під гравцем
	if (ray_cast_bottom.is_colliding() and 
	ray_cast_bottom.get_collider().is_in_group("floor") and 
	!ray_cast_bottom.get_collider().is_in_group("player")):
		
		$Camera2D.limit_bottom =(ray_cast_bottom.get_collision_point().y + 20)
		
		# Чи знаходиться гравець на нижноьому кордоні рінвя
		if ray_cast_bottom.get_collider().is_in_group("end_floor"):
			end_fl_collide = true
	else:
		$Camera2D.limit_bottom = 10000000

	if (ray_cast_left.is_colliding() and 
	ray_cast_left.get_collider().is_in_group("end_wall") and 
	!ray_cast_left.get_collider().is_in_group("player")):
		
		$Camera2D.limit_left =(ray_cast_left.get_collision_point().x - 10)
		
	if (ray_cast_right.is_colliding() and 
	ray_cast_right.get_collider().is_in_group("end_wall") and 
	ray_cast_right.get_collider().is_in_group("player")):
		
		$Camera2D.limit_right =(ray_cast_right.get_collision_point().x + 10)


func dialog_start(dial_info, dial_id = 0):
	if !in_dial or dial_id != 0:
		can_move = false
		dial_size = 0
		in_dial = true
		print(dial_max_stage)
		print(dial_stage)
		if !dial_id:
			for i in dial_info[0]:
				dial_max_stage += 1
				print(dial_max_stage)
		$CanvasLayer/NinePatchRect.visible = true
		$CanvasLayer/NinePatchRect/Name.text = dial_info[0][dial_stage]
		$CanvasLayer/NinePatchRect/Say.text = dial_info[1][dial_stage]
		if !dial_stage:
			$Camera2D.move_local_x(400)
		for i in dial_info[1][dial_stage-1]:
			dial_size += 1
		$CanvasLayer/NinePatchRect/Say/open.start()
		dial_stage += 1
		dial_now = dial_info


func bag_pos():
	return $sprite/bag.global_position


func coin_take(type,amount = 1):
	if type == "Gold":
		gold += amount * 100
		var effect = coin_effect.instance()
		main.get_node("effects").add_child(effect)
		effect.global_position = bag_pos()
	if type == "Crystal":
		gold += amount
	if type == "Ether":
		ether += amount
		var effect = coin_effect.instance()
		main.get_node("effects").add_child(effect)
		effect.global_position = bag_pos()
		effect.modulate.r8 = 255
		effect.modulate.g8 = 0
		effect.modulate.b8 = 0

func _physics_process(delta):
	stats_update()
	camera_limiter()
	movement(delta)
	anim_update()
	attack()
	die()
	



func _on_DashTime_timeout():
	dash = false



func _ready():
	pass

func _on_open_timeout():
	$CanvasLayer/NinePatchRect/Say.visible_characters += 1
	if $CanvasLayer/NinePatchRect/Say.visible_characters != dial_size+ round(dial_size*0.5):
		$CanvasLayer/NinePatchRect/Say/open.start(1.0/dial_speed)
	elif dial_stage < dial_max_stage:
		$CanvasLayer/NinePatchRect/Say.visible_characters = 0
		dialog_start(dial_now,1)

	elif dial_stage == dial_max_stage:
		$Camera2D.move_local_x(-400)
		$CanvasLayer/NinePatchRect/Say/close.start(1)


func _on_close_timeout():
	$CanvasLayer/NinePatchRect/Say.visible_characters = 0
	$CanvasLayer/NinePatchRect.visible = false
	in_dial = false
	dial_stage = 0
	dial_max_stage = 0
	dial_now = null
	can_move = true

func attack():
	if Input.is_action_just_pressed("attack"):
		var current_damage
		if weapon == {}:
			return
		current_damage = weapon["Damage"] * dmg_scale["All"]
		if !on_cooldown:
			if weapon["Type"] == "Spell" and stamina >= weapon["Stamina"]:
				var projectile = spell.instance()
				projectile.direction = $sprite.scale.x
				main.get_node("effects").add_child(projectile)
				projectile.position.x = $sprite/Node2D2.global_position.x + 10
				projectile.position.y = $sprite/Node2D2.global_position.y - 30
				projectile.dmg = current_damage
				on_cooldown = true
				stamina -= 10
				$WeaponCooldown.start(weapon["Cooldown"])
	
func take_dmg(dmg_t):
	health -= dmg_t

func _on_WeaponCooldown_timeout():
	on_cooldown = false


func _on_Recovery_timeout():
	if stamina < max_stamina:
		stamina += 0.5

func die():
	if health < 1:
		get_tree().reload_current_scene()
