extends KinematicBody2D


export var hp = 100
export var lvl = 1
export var dmg = 10
export var att = false
var velocity = Vector2()
var gravity = 10000
var cd = false
var die = false

onready var eff = preload("res://effects/Lable effect.tscn")
onready var main = get_tree().get_root().get_node("Main")

func attack():
	att = true
	$AnimationPlayer.play("attack")
	$ColorRect.visible = true
	$ColorRect/Area2D/CollisionShape2D.disabled = false
	$attack.start()
	

func take_damage(dmg):
	hp -= dmg
	var dmg_eff = eff.instance()
	main.get_node("effects").add_child(dmg_eff)
	dmg_eff.global_position = global_position
	dmg_eff.global_position.y -= 100

func _physics_process(delta):
	velocity.y = gravity * delta
	move_and_slide(velocity,Vector2.UP)
	if $RayCast2D.is_colliding():
		if $RayCast2D.get_collider().is_in_group("player"):
			if !cd and !att and !die:
				attack()
	if $back.is_colliding():
		if $back.get_collider().is_in_group("player"):
			apply_scale(Vector2(-1,1))
	die()


func die():
	if hp < 1 and !die:
		die = true
		$AnimationPlayer.play("die")
		$die.start()


func _on_attack_timeout():
	att = false
	$ColorRect.visible = false
	$ColorRect/Area2D/CollisionShape2D.disabled = true
	$AnimationPlayer.play("idle")
	$CD.start()
	cd = true


func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		body.take_dmg(dmg)


func _on_CD_timeout():
	cd = false


func _on_die_timeout():
	queue_free()
