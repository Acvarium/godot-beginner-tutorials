extends CanvasLayer


onready var main = get_tree().get_root().get_node("Main")


func exit():
	get_tree().quit()


func pause():
	get_tree().paused = !get_tree().paused
	$PauseMenu.visible = !$PauseMenu.visible

func _input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		pause()

func _ready():
	pass # Replace with function body.


func _on_Button_button_up():
	pause()


func _on_ButtonExit_button_up():
	exit()
