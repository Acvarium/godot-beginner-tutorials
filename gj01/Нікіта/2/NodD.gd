extends Node2D



var stage = 0
var times = 100

	

func new_stage(st):
	stage = st
	get_tree().paused = true
	$CanvasLayer/ColorRect.visible = true
	$Timervs.start()


func _on_Timer_timeout():
	if !stage:
		$Label.visible_characters += 1
		times -= 1
	else:
		get_node(str("Label",stage+1)).visible_characters += 1
		times -= 1
	if !times:
		times = 20
		$CanvasLayer/ColorRect.visible = false
		get_tree().paused = false
	else:
		$Timervs.start()
		
		

func _on_TimerDS_timeout():
	$Timervs.start()
	get_tree().paused = true
	$CanvasLayer/ColorRect.visible = true


func _on_S1_body_entered(body):
	new_stage(1)
	$S1.queue_free()


func _on_S2_body_entered(body):
	new_stage(2)
	$S2.queue_free()



func _on_S3_body_entered(body):
	new_stage(3)
	$S3.queue_free()
	

func _on_S4_body_entered(body):
	new_stage(4)
	$S4.queue_free()


func _on_S5_body_entered(body):
	new_stage(5)
	$S5.queue_free()



func _on_S6_body_entered(body):
	new_stage(6)
	$S6.queue_free()
