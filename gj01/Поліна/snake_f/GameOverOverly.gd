extends CanvasLayer



func _ready():
	get_node("VBoxContainer/ScoreLabel").text += str(Globals.highscore)

func _on_Button_pressed():
	Globals.reset_game()
	queue_free()
