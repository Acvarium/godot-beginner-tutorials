class_name Invader
extends KinematicBody2D

export(Resource) var bullet_prefab
export var velocity := Vector2.ZERO

signal is_shot

func destroy():
	emit_signal("is_shot", global_position)
	queue_free()

func _physics_process(_delta):
	var _v = move_and_slide(velocity * get_viewport_rect().size.x)

func _on_Area2D_body_entered(bullet):
	bullet.destroy()
	destroy()

func drop_bullet():
	var bullet = bullet_prefab.instance()
	bullet.global_position = global_position + Vector2(0, 20)
	
	return bullet
