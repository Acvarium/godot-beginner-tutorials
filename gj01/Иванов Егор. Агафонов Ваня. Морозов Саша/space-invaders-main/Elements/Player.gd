extends KinematicBody2D

export var player := 1
export var speed := 0.5
export var _available_bullets := 1
var bullet_prefab = preload("res://Elements/Player_Bullet.tscn")
var explosions = [preload("res://Elements/Explosion_0.tscn"), preload("res://Elements/Explosion_1.tscn"), preload("res://Elements/Explosion_2.tscn"), preload("res://Elements/Explosion_3.tscn")]
var alive := true

signal is_shot


func launch_bullet():
	var bullet = bullet_prefab.instance()
	bullet.global_position = global_position + Vector2(0, -20)
	owner.add_child(bullet)
	bullet.connect("on_destroy", self, "reset_bullet")
	_available_bullets -= 1

func reset_bullet():
	_available_bullets += 1


func is_shot():
	emit_signal("is_shot")
	alive = false
	$Ship_1.visible = false
	$Ship_Destroyed.visible = true
	$Dead.play()
	for i in rand_range(10, 20):
		yield(get_tree().create_timer(0.1), "timeout")
		var boom = explosions[randi() % explosions.size()].instance()
		add_child(boom)
		boom.global_position = global_position + Vector2(rand_range(-75, 75), rand_range(-75, 75))
		boom.global_scale = Vector2.ONE 


func _physics_process(_delta):
	if alive:
		var velocity = Vector2.ZERO
		
		# Shoot
		if Input.is_action_just_pressed("p_shoot") and _available_bullets > 0:
			launch_bullet()
		
		# Move
		if Input.is_action_pressed("p_left"):
			velocity.x = -speed * get_viewport_rect().size.x * Input.get_action_strength("p_left")
		elif Input.is_action_pressed("p_right"):
			velocity.x = speed * get_viewport_rect().size.x * Input.get_action_strength("p_right")
		
		velocity = move_and_slide(velocity)

func _on_Dead_finished():
	queue_free()

func _on_Area2D_body_entered(bullet):
	if alive:
		bullet.destroy()
		is_shot()
