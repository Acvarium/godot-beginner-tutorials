extends Node2D

func _on_AnimatedSprite_animation_finished():
	$AnimatedSprite.visible = false
	$Explosion.visible = false

func _on_AudioStreamPlayer_finished():
	queue_free()
