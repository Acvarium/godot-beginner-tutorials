extends KinematicBody2D
class_name Bullet

export var velocity := Vector2.ZERO

signal on_destroy

func _physics_process(_delta):
	var _v = move_and_slide(velocity * -get_viewport_rect().size.y)

func destroy():
	emit_signal("on_destroy")
	queue_free()



