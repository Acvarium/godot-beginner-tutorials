extends KinematicBody2D
const DEFAULT_SPEED = 500.0
var speed = DEFAULT_SPEED
var speed_up_value = 15.0
var direction = Vector2.DOWN
var velocity = Vector2()
var stuck_to_paddle = false
export var have_timeout = false
onready var main_node = get_tree().get_root().get_node("Main")
onready var default_collision_layer = collision_layer


func _ready():
	velocity = speed * direction
	collision_layer


func reset_collision_layer():
	collision_layer = default_collision_layer
	

func speed_up():
	if speed == DEFAULT_SPEED:
		return
	speed = clamp(speed + speed_up_value, speed, DEFAULT_SPEED)


func reset_speed(slow_down = 1.0):
	speed = DEFAULT_SPEED * slow_down
	if velocity.length() == 0:
		velocity = direction * speed
	else:
		velocity = velocity.normalized() * speed


func half_speed():
	speed = speed / 2


func _physics_process(delta):
	if stuck_to_paddle:
		return
	var collision = move_and_collide(velocity * delta)
	if collision:
		if collision.collider.name == "Paddle":
			
			var player = collision.collider
			var x_distance = global_position.x - player.global_position.x
			var width = player.get_width()
			var relation = clamp(x_distance / width, -1, 1)
			var angle = 45 * relation
			var angle_rad = deg2rad(angle)
			velocity = Vector2.UP.rotated(angle_rad) * velocity.length()
			stuck_to_paddle = player.stick_ball(self)
			if stuck_to_paddle:
				main_node.add_effect("StickHit", global_position)
			else:
				main_node.add_effect("PaddleHit", global_position)
		else:
			velocity = velocity.bounce(collision.normal)
			if collision.collider.is_in_group("brick"):
				collision.collider.queue_free()
			elif collision.collider.has_method("brick_hit"):
				collision.collider.brick_hit()
			else:
				main_node.add_effect("WallHit", global_position)
		speed_up()
	velocity = velocity.normalized() * speed
	
