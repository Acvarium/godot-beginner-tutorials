extends Node2D
var brick_file = preload("res://bricks/Brick.tscn")
var bonus_file = preload("res://bricks/Bonus.tscn")
var ball_file = preload("res://Ball.tscn")
var levels_base_path = "res://levels/Level"

var tile_map_path = ""
var bonus_hit_counter = 100
var max_bonus_hits = 10
onready var ball_start_pos = $BallSpawnPos.global_position
var mouse_over_menu_button = false
var mouse_captured_mode = false
var life_bonus_was_created = false

var next_bonus = -1

func load_level():
	if Global.current_level == -1:
		return
	$LevelHolder/Level.free()
	var level_load = load(levels_base_path + str(
		Global.current_level).pad_zeros(3) + ".tscn")
		
	var level = level_load.instance()
	$LevelHolder.add_child(level)
	if level.has_node("UI"):
		level.get_node("UI").free()
	tile_map_path = level.get_node("TileMap").get_path()


func _on_Bottom_body_entered(body):
	if body.is_in_group("ball"):
		body.queue_free()
		if $Balls.get_child_count() <= 1:
			spawn_ball()
			Global.balls -= 1
			if Global.balls <= 0:
				Global.balls = 0
				game_over()
		$Paddle.reset()
		destroy_bonuses()
		update_lives_view()


func update_lives_view():
	$CanvasLayer/LivesBlock/Ball1.visible = Global.balls > 0
	$CanvasLayer/LivesBlock/Ball2.visible = Global.balls > 1
	$CanvasLayer/LivesBlock/Ball3.visible = Global.balls > 2
	$CanvasLayer/LivesBlock/Plus.visible = Global.balls > 3


func check_for_victory():
	var dist_bricks = 0
	for b in $Bricks.get_children():
		if !b.indistractable:
			dist_bricks += 1
	if dist_bricks == 0:
		show_level_complition_menu()


func spawn_ball():
	var new_ball = ball_file.instance()
	$Balls.add_child(new_ball)
	new_ball.global_position = ball_start_pos
	new_ball.speed = 0
	$BallStartTimer.start()


func _ready():
	randomize()
	load_level()
	gen_level_from_tiles()
	update_score(Global.current_score)
	bonus_hit_counter = randi() % max_bonus_hits + 3
	spawn_ball()
	update_lives_view()


func gen_level_from_tiles():
	var tilemap : TileMap = get_node_or_null(tile_map_path)
	if !tilemap:
		tilemap = $LevelHolder/Level/TileMap
	for b in $Bricks.get_children():
		b.free()
	for t in tilemap.get_used_cells():
		var cell_id: int = tilemap.get_cellv(t)
		print(tilemap.map_to_world(t))
		var brick_pos = tilemap.map_to_world(t) + tilemap.global_position
		add_block(brick_pos, cell_id)
	tilemap.visible = false


func add_block(pos, id):
	var new_brick = brick_file.instance()
	$Bricks.add_child(new_brick)
	new_brick.global_position = pos
	new_brick.set_type(id)


func spawn_bonus(pos):
	var bonus_num = 7
	if life_bonus_was_created:
		bonus_num = 6
	var bonus_id = randi() % bonus_num - 1
	if next_bonus != -1:
		bonus_id = next_bonus
	next_bonus = -1
	if $Balls.get_child_count() > 1:
		return
	var bonus_inst = bonus_file.instance()
	$Bonuses.add_child(bonus_inst)
	bonus_inst.id_changed(bonus_id)
	bonus_inst.global_position = pos
	if bonus_id == 6:
		life_bonus_was_created = true


func bonus_collected(bonus_id):
	if bonus_id == 4:
		$Break.visible = true
	if bonus_id == 3:
		for b in $Balls.get_children():
			if b.has_method("half_speed"):
				b.half_speed()
	elif bonus_id == 5:
		destroy_bonuses()
		multiple_balls()
	elif bonus_id == 6:
		Global.balls += 1
		update_lives_view()


func add_effect(effect_name, pos):
	$EffectsSpawner.add_effect(effect_name, pos)

func destroy_bonuses():
	for b in $Bonuses.get_children():
		b.queue_free()


func add_score(value, pos):
	Global.current_score += value
	update_score(Global.current_score)
	bonus_hit_counter -= 1
	if bonus_hit_counter <= 0 or next_bonus != -1:
		bonus_hit_counter = randi() % max_bonus_hits + 1
		spawn_bonus(pos)
	$WinCheckTimer.start()


func multiple_balls():
	if $Balls.get_child_count() > 1:
		return
	var current_ball = $Balls.get_child(0)
	current_ball.reset_speed()
	for i in range(2):
		var b2 = ball_file.instance()
		$Balls.add_child(b2)
		b2.speed = current_ball.speed
		b2.global_position = current_ball.global_position
		b2.velocity = current_ball.velocity.rotated((randf() - 0.5) * 2)


func capture_mouse(to_capture):
	if mouse_over_menu_button:
		to_capture = false
	if to_capture:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	mouse_captured_mode = to_capture


func _input(event):
	if event.is_action_pressed("test"):
		multiple_balls()
	elif event.is_action_pressed("restart"):
		get_tree().reload_current_scene()
		
	if event.is_action_pressed("LMB"):
		capture_mouse(true)
	elif event.is_action_pressed("RMB") or event.is_action_pressed("esc"):
		capture_mouse(false)
	if event.is_action_pressed("menu"):
		_on_IngameMenuButton_pressed()
	if event.is_action_pressed("t1"):
		next_bonus = 0
	if event.is_action_pressed("t2"):
		next_bonus = 1
	if event.is_action_pressed("t3"):
		next_bonus = 2
	if event.is_action_pressed("t4"):
		next_bonus = 3
	if event.is_action_pressed("t5"):
		next_bonus = 4
	if event.is_action_pressed("t6"):
		next_bonus = 5
	if event.is_action_pressed("t7"):
		next_bonus = 6


func update_score(score):
	$CanvasLayer/Score.text = str(score).pad_zeros(6)
	$CanvasLayer/LevelCompliteMenu/M/G/Score.text = str(score).pad_zeros(6)
	$CanvasLayer/GameOverMenu/M/G/Score.text = str(score).pad_zeros(6)


func _on_BallStartTimer_timeout():
	for b in $Balls.get_children():
		b.reset_speed(0.5)


func _on_TextureButton_mouse_entered():
	mouse_over_menu_button = true


func _on_TextureButton_mouse_exited():
	mouse_over_menu_button = false


func _on_QuitButton_pressed():
	get_tree().quit()


func _on_IngameMenuButton_pressed():
	capture_mouse(false)
	$CanvasLayer/IngameMenu.visible = true
	$CanvasLayer/LevelCompliteMenu.visible = false
	$CanvasLayer/GameOverMenu.visible = false
	get_tree().paused = true


func show_level_complition_menu():
	capture_mouse(false)
	$CanvasLayer/IngameMenu.visible = false
	$CanvasLayer/LevelCompliteMenu.visible = true
	$CanvasLayer/GameOverMenu.visible = false
	get_tree().paused = true


func game_over():
	capture_mouse(false)
	get_tree().paused = true
	$CanvasLayer/IngameMenu.visible = false
	$CanvasLayer/LevelCompliteMenu.visible = false
	$CanvasLayer/GameOverMenu.visible = true


func _on_BackToGameButton_pressed():
	$CanvasLayer/IngameMenu.visible = false
	$CanvasLayer/LevelCompliteMenu.visible = false
	$CanvasLayer/GameOverMenu.visible = false
	get_tree().paused = false


func _on_GoToMainMenuButton_pressed():
	get_tree().paused = false
	Global.goto_scene("res://MainMenu.tscn")


func _on_NextLevelButton_pressed():
	Global.open_next_level()


func _on_WinCheckTimer_timeout():
	check_for_victory()


func _on_MainMenuButton_pressed():
	get_tree().paused = false
	Global.goto_scene("res://MainMenu.tscn")


func _on_BreakArea_body_entered(body):
	if $Break.visible:
		if body.name == "Paddle":
			Global.current_score += 10000
			show_level_complition_menu()
