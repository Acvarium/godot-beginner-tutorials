extends KinematicBody2D
var speed = 300.0
var mouse_movement = Vector2()

var sticky = false
onready var main_node = get_tree().get_root().get_node("Main")
enum ANIM_STATES {normal, long, gun}
var current_anim_state = ANIM_STATES.normal
var next_anim_state = ANIM_STATES.normal

func _ready():
	pass 


func get_width():
	return $WidthCheck.position.x


func _physics_process(delta):
	var input = Input.get_action_strength("ui_right") - \
		Input.get_action_strength("ui_left")
	
	if main_node.mouse_captured_mode:
		input = mouse_movement.x
	var run_mult = 1.0
	if Input.is_action_pressed("run"):
		run_mult = 2.0
	var velocity = Vector2(input * speed * run_mult, 0)
	move_and_collide(velocity * delta)
	mouse_movement = Vector2()
	if Input.is_action_pressed("LMB") and current_anim_state == ANIM_STATES.gun:
		if $FireTimer.time_left == 0 and !$AnimationPlayer.is_playing():
			fire()

func reset():
	next_anim_state = ANIM_STATES.normal
	sticky = false
	update_animation()

func collect_bonus(bonus_id):
	bonus_sound(bonus_id)
	main_node.bonus_collected(bonus_id)
	if bonus_id == 0:
		next_anim_state = ANIM_STATES.gun
	elif bonus_id == 1:
		next_anim_state = ANIM_STATES.long
	elif bonus_id != 6:
		next_anim_state = ANIM_STATES.normal
		
	if sticky and bonus_id != 2:
		sticky = false
	if bonus_id == 2:
		sticky = true
	else:
		unstick_balls()
	update_animation()


func fire():
	main_node.add_effect("Bullet", $GunLeft.global_position)
	main_node.add_effect("Bullet", $GunRight.global_position)
	main_node.add_effect("Blaster", global_position)
	$FireTimer.start()

func bonus_sound(bonus_id):
	if bonus_id >= 0 and bonus_id < 7:
		$Sounds.get_node(str(bonus_id)).play()


func update_animation():
	if current_anim_state == next_anim_state:
		return
	if current_anim_state == ANIM_STATES.normal:
		if next_anim_state == ANIM_STATES.long:
			$AnimationPlayer.play("enlarge")
		elif next_anim_state == ANIM_STATES.gun:
			$AnimationPlayer.play("gun")
	elif current_anim_state == ANIM_STATES.long:
		if next_anim_state == ANIM_STATES.normal:
			$AnimationPlayer.play_backwards("enlarge")
		elif next_anim_state == ANIM_STATES.gun:
			$AnimationPlayer.play("shring_to_gun")
	elif current_anim_state == ANIM_STATES.gun:
		if next_anim_state == ANIM_STATES.normal:
			$AnimationPlayer.play_backwards("gun")
		elif next_anim_state == ANIM_STATES.long:
			$AnimationPlayer.play_backwards("shring_to_gun")
	current_anim_state = next_anim_state


func stick_ball(ball):
	if !sticky:
		return false
	$Holders/BallHolder.global_position = ball.global_position
	$Holders/BallHolder.remote_path = ball.get_path()
	ball.collision_layer = 0
	$StickTimer.start()
	return true


func unstick_balls():
	$StickTimer.stop()
	var ball = get_node_or_null($Holders/BallHolder.remote_path)
	if !ball:
		return
	ball.stuck_to_paddle = false
	ball.reset_collision_layer()
	$Holders/BallHolder.remote_path = ""


func _input(event):
	if event is InputEventMouseMotion:
		mouse_movement = event.relative
	if event.is_action_pressed("LMB"):
		unstick_balls()

func _on_StickTimer_timeout():
	unstick_balls()
