extends Node2D

func _ready():
	pass 

func add_effect(effect_name, pos):
	var effect_load = load("res://effects/" + effect_name + ".tscn")
	var effect = effect_load.instance()
	add_child(effect)
	effect.global_position = pos
