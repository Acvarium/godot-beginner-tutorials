extends TextureButton
onready var main_menu = get_tree().get_root().get_node("MainMenu")

var level_id = -1

func _ready():
	pass 

func set_id(id):
	level_id = id
	$Label.text = "Level " + str(id).pad_zeros(2)

func _on_LevelSelectionButton_pressed():
	Global.open_level(level_id)
	
