extends Node2D
export var random_pitch_offset = 0.0

func _ready():
	randomize()
	if has_node("AudioStreamPlayer2D"):
		var pitch_offset = (randf() - 0.5) * random_pitch_offset
		$AudioStreamPlayer2D.pitch_scale = 1 + pitch_offset
		$AudioStreamPlayer2D.play() 

func _on_Timer_timeout():
	queue_free()
