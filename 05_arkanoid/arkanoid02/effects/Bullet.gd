extends Area2D
var speed = 500.0

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	position.y -= speed * delta


func _on_Bullet_body_entered(body):
	if body.has_method("brick_hit"):
		body.brick_hit()
		queue_free()
