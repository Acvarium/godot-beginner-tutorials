extends StaticBody2D
var indistractable = false
var hit_points = 1
func _ready():
	randomize()
	$Cracks.flip_h = randi() % 2
	$Cracks.flip_v = randi() % 2
onready var main_node = get_tree().get_root().get_node("Main")
var score_values = [50, 60, 70, 80, 90, 100, 110, 120, 150]


func set_type(id):
	$Sprite.frame = id
	indistractable = id == 9
	if id == 8:
		hit_points = 4


func brick_hit(value = 1):
	if indistractable:
		main_node.add_effect("GoldHit", global_position)
		return
	if $Sprite.frame == 8:
		main_node.add_effect("MetalHit", global_position)
	else:
		main_node.add_effect("Hit", global_position)
	hit_points -= value
	if hit_points <= 0:
		main_node.add_score(score_values[$Sprite.frame], global_position)
		queue_free()
	else:
		if $Sprite.frame == 8:
			$Cracks.frame = 4 - hit_points
