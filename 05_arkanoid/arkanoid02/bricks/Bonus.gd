tool
extends Area2D
export var bonus_id = 0 setget id_changed
export var frame = 0 setget frame_changed
var speed = 100

func frame_changed(value):
	if has_node("Sprite"):
		frame = value % $Sprite.vframes
		$Sprite.frame_coords = Vector2(bonus_id, frame)


func id_changed(value):
	if has_node("Sprite"):
		bonus_id = value
		$Sprite.frame_coords = Vector2(bonus_id, frame)


func _ready():
	pass


func _physics_process(delta):
	if Engine.editor_hint:
		return
	position.y += speed * delta
	if global_position.y > get_viewport_rect().size.y + 100:
		queue_free()


func _on_Bonus_body_entered(body):
	if Engine.editor_hint:
		return
	if body.has_method("collect_bonus"):
		body.collect_bonus(bonus_id)
		queue_free()
