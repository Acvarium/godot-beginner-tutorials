extends Control

onready var level_menu_grid = $LevelSelectMenu/M/S/G
var level_button_file = preload("res://LevelSelectionButton.tscn")


func _ready():
	Global.balls = 2
	generate_level_selection_buttons()
	Global.current_score = 0

func generate_level_selection_buttons():
	for i in range(Global.max_level):
		var level_button = level_button_file.instance()
		level_menu_grid.add_child(level_button)
		level_button.set_id(i + 1)

func _on_QuitButton_pressed():
	get_tree().quit()


func _on_NewGameButton_pressed():
	Global.current_level = 1
	Global.goto_scene("res://Main.tscn")


func show_main_menu():
	$LevelSelectMenu.visible = false
	$MainMenu.visible = true
	$Label.text = "MENU"


func show_level_selection():
	$LevelSelectMenu.visible = true
	$MainMenu.visible = false
	$Label.text = "LEVELS"


func _on_BackButton_pressed():
	show_main_menu()


func _on_SelectLevelButton_pressed():
	show_level_selection()
